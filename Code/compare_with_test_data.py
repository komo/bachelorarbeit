from pathlib import Path
from src.metrics import *
import numpy as np
import matplotlib.pyplot as plt
from src.gp_inference import performGP, neg_log_likelihood, length_scale
from src.forward_model import get_training_emission, get_measurement
from src.optimize import optimize_params
from src.plot_helper import plot_emission
from src.error_model import get_std


def analyse_all_tests(  transfer_matrix, 
                        covariance_function, 
                        params_initial, 
                        filename_template="stationary_volume_{}.png",
                        title = "stationary volume", 
                        file_path = "data/results_comparison/stationary_volume/",
                        is_nonstationary = False,
                        rescale = False):

    nrmse_data = []
    psnr_data = []
    ssim_data = []
    rdte_data = []
    length_scale_data = []

    for i in range(1,74):
        print("Analysing test emission NN_ {}".format(i))
        original = get_training_emission(i)
        max_emiss = original.max()/1000000
        scale_factor = 1.0
        if max_emiss < 2 and rescale:
            scale_factor = 2.0/max_emiss
            print(scale_factor)
            original = original * scale_factor

        measurement = get_measurement(original, transfer_mat=transfer_matrix)
        params = optimize_params(measurement, params_initial, transfer_matrix, covariance_function)
        #params[1] = 0.16 # only for fixed length scale
        fit, _ = performGP(measurement, params, transfer_mat=transfer_matrix, cov_func=covariance_function, format="image")        

        if max_emiss < 2 and rescale:
            fit = fit/scale_factor
            original = original/scale_factor

        nrmse_fit = nrmse(original, fit)
        psnr_fit = psnr(original, fit, data_range=original.max() - original.min())
        ssim_fit = ssim(original, fit, data_range=original.max() - original.min())
        rdte_fit = rdte(original, fit)

        print(nrmse_fit, psnr_fit, ssim_fit)

        nrmse_data.append(nrmse_fit)
        psnr_data.append(psnr_fit)
        ssim_data.append(ssim_fit)
        rdte_data.append(rdte_fit)
        length_scale_data.append(params[1])

        if is_nonstationary:
            fig, (ax1, ax2, ax3) = plt.subplots(1,3, figsize=(15,7))
            fig.text(0.15, 0.96, title + " NN_{}".format(i), fontsize=14)

            plot_emission(original, fig, ax1, title="Original")
            title_fit = "Fit;   sigma_E: {};  p1: {}; \n p2: {};  p3: {};  p4: {};\n NMSRE: {};  PSNR: {};  SSIM: {};" \
                .format(round(params[0]),round(params[1],2), round(params[2],2), 
                    round(params[3],2), round(params[4],2),round(nrmse_fit, 2),
                    round(psnr_fit, 1),round(ssim_fit, 2))

            scales = np.zeros((42,23))
            z_coords = np.loadtxt("data/z_coordinates_reduced.txt")
            scales_z = np.array([length_scale(z, params) for z in z_coords])
            print(z_coords.shape)

            for j in range(23):
                scales[:,j]= scales_z

            plot_emission(fit, fig, ax2, title=title_fit, limits=[0, original.max()])
            plot_emission(scales, fig, ax3, title = "length scales", interpol=None)
            plt.tight_layout()
            plt.savefig(file_path + filename_template.format(i))
        else:
            fig, (ax1, ax2) = plt.subplots(1,2, figsize=(10,7))
            fig.text(0.15, 0.96, title + " NN_{}".format(i), fontsize=14)

            plot_emission(original, fig, ax1, title="Original")
            title_fit = "Fit;   sigma_E: {};  l: {};  RDTE: {}; \n NMSRE: {};  PSNR: {};  SSIM: {};" \
                .format(round(params[0]),round(params[1],2), round(rdte_fit*100,2),round(nrmse_fit, 2),round(psnr_fit, 1),round(ssim_fit, 2))

            plot_emission(fit, fig, ax2, title=title_fit, limits=[0, original.max()])
            plt.tight_layout()
            plt.savefig(file_path + filename_template.format(i))
            plt.close(fig)



    return nrmse_data, psnr_data, ssim_data, rdte_data, length_scale_data

# nrmse_results1, psnr_results1, ssim_results1, rdte_results1, length_scale_results1 = analyse_all_tests("volume", "simple_rbf", [400000, 0.15], 
#                         filename_template="stationary_volume_fixed_length_{}.png",
#                         title = "stationary volume", 
#                         file_path = "data/results_comparison/stationary_volume_fixed_length/",
#                         is_nonstationary=False)


# data_to_save = np.array([nrmse_results1, psnr_results1, ssim_results1, rdte_results1, length_scale_results1])
# np.savetxt("data/results_comparison/stationary_volume_fixed_length/compare_results_stationary_volume.txt", data_to_save.T)


# nrmse_results5, psnr_results5, ssim_results5, rdte_results5, length_scale_results5 = analyse_all_tests("volume", "simple_rbf", [400000, 0.15], 
#                         filename_template="stationary_volume_rescaled_{}.png",
#                         title = "stationary volume with rescale", 
#                         file_path = "data/results_comparison/stationary_volume_rescaled/",
#                         is_nonstationary=False, 
#                         rescale=True)


# data_to_save = np.array([nrmse_results5, psnr_results5, ssim_results5, rdte_results5, length_scale_results5])
# np.savetxt("data/results_comparison/stationary_volume_rescaled/compare_results_stationary_volume_rescaled.txt", data_to_save.T)


# nrmse_results2, psnr_results2, ssim_results2, rdte_results2, length_scale_results2 = analyse_all_tests("line", "simple_rbf", [400000, 0.15], 
#                         filename_template="stationary_line_{}.png",
#                         title = "stationary volume with rescale", 
#                         file_path = "data/results_comparison/stationary_line/",
#                         is_nonstationary=False)

# data_to_save = np.array([nrmse_results2, psnr_results2, ssim_results2, rdte_results2, length_scale_results2])
# np.savetxt("data/results_comparison/stationary_line/compare_results_stationary_line.txt", data_to_save.T)


nrmse_results3, psnr_results3, ssim_results3, rdte_results3, _ = analyse_all_tests("volume", "rbf_sigmoid_z", [250000, 0.17, 0.01, -0.7, -5], 
                        filename_template="nonstationary_volume_rescale_{}.png",
                        title = "nonstationary volume", 
                        file_path = "data/results_comparison/nonstationary_volume_v2/",
                        is_nonstationary=True,
                        rescale=True)

data_to_save = np.array([nrmse_results3, psnr_results3, ssim_results3, rdte_results3])
np.savetxt("data/results_comparison/nonstationary_volume_v2/compare_results_nonstationary_volume.txt", data_to_save.T)


# nrmse_results4, psnr_results4, ssim_results4 = analyse_all_tests("line", "rbf_sigmoid_z", [250000, 0.17, 0.01, -0.7, -5], 
#                         filename_template="nonstationary_line_{}.png",
#                         title = "nonstationary line", 
#                         file_path = "data/results_comparison/nonstationary_line/",
#                         is_nonstationary=True)

# data_to_save = np.array([nrmse_results4, psnr_results4, ssim_results4])
# np.savetxt("data/results_comparison/nonstationary_line/compare_results_nonstationary_line.txt", data_to_save.T)


# nrmse_results1 = np.array(nrmse_results1)
# psnr_results1 = np.array(psnr_results1)
# ssim_results1 = np.array(ssim_results1)
# rdte_results1 = np.array(rdte_results1)
# length_scale_results1 = np.array(length_scale_results1)

# nrmse_results2 = np.array(nrmse_results2)
# psnr_results2 = np.array(psnr_results2)
# ssim_results2 = np.array(ssim_results2)
# rdte_results2 = np.array(rdte_results2)
# length_scale_results2 = np.array(length_scale_results2)

# nrmse_results5 = np.array(nrmse_results5)
# psnr_results5 = np.array(psnr_results5)
# ssim_results5 = np.array(ssim_results5)
# rdte_results5 = np.array(rdte_results5)
# length_scale_results5 = np.array(length_scale_results5)

nrmse_results3 = np.array(nrmse_results3)
psnr_results3 = np.array(psnr_results3)
ssim_results3 = np.array(ssim_results3)
rdte_results3 = np.array(rdte_results3)

# nrmse_results4 = np.array(nrmse_results4)
# psnr_results4 = np.array(psnr_results4)
# ssim_results4 = np.array(ssim_results4)

print()

# print("stationary volume")
# print(nrmse_results1.min(), nrmse_results1.max(), np.mean(nrmse_results1))
# print(psnr_results1.min(), psnr_results1.max(), np.mean(psnr_results1))
# print(ssim_results1.min(), ssim_results1.max(), np.mean(ssim_results1))
# print(rdte_results1.min(), rdte_results1.max(), np.mean(rdte_results1))
# print(length_scale_results1.min(), length_scale_results1.max(), length_scale_results1.mean())

# print("stationary line")
# print(nrmse_results2.min(), nrmse_results2.max(), np.mean(nrmse_results2))
# print(psnr_results2.min(), psnr_results2.max(), np.mean(psnr_results2))
# print(ssim_results2.min(), ssim_results2.max(), np.mean(ssim_results2))
# print(rdte_results2.min(), rdte_results2.max(), np.mean(rdte_results2))
# print(length_scale_results2.min(), length_scale_results2.max(), length_scale_results2.mean())

# print("stationary volume rescaled")
# print(nrmse_results5.min(), nrmse_results5.max(), np.mean(nrmse_results5))
# print(psnr_results5.min(), psnr_results5.max(), np.mean(psnr_results5))
# print(ssim_results5.min(), ssim_results5.max(), np.mean(ssim_results5))
# print(rdte_results5.min(), rdte_results5.max(), np.mean(rdte_results5))
# print(length_scale_results5.min(), length_scale_results5.max(), length_scale_results5.mean())


print("nonstationary volume")
print(nrmse_results3.min(), nrmse_results3.max(), np.mean(nrmse_results3))
print(psnr_results3.min(), psnr_results3.max(), np.mean(psnr_results3))
print(ssim_results3.min(), ssim_results3.max(), np.mean(ssim_results3))
print(rdte_results3.min(), rdte_results3.max(), np.mean(rdte_results3))

# # print("nonstationary line")
# print(nrmse_results4.min(), nrmse_results4.max(), np.mean(nrmse_results4))
# print(psnr_results4.min(), psnr_results4.max(), np.mean(psnr_results4))
# print(ssim_results4.min(), ssim_results4.max(), np.mean(ssim_results4))
