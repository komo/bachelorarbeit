#include "Eigen/Dense"
#include <math.h>
#include <iostream>
using namespace std;
using namespace Eigen;

#define SIGMA 1.0
#define L 1.0

float covarianceFunction(float x_i, float x_j) {
    float sqdist = (x_i - x_j) * (x_i - x_j);
    float arg = -0.5 * sqdist / L;
    return SIGMA * SIGMA * exp(arg);
}

MatrixXf generateCovMatrix(float *vector1, float *vector2, int dim1, int dim2) {
    
    MatrixXf cov_matrix(dim1, dim2);

    for (int i = 0; i < dim1; i++) {
        for (int j = 0; j < dim2; j++) {
            cov_matrix(i,j) = covarianceFunction(vector1[i], vector2[j]);
        }
    }

    return cov_matrix;
}

MatrixXf calcPosteriorCov(MatrixXf k_x_x, MatrixXf k_xFit_x, MatrixXf k_xFit_xFit) {
    
    return k_xFit_xFit - k_xFit_x * k_x_x.inverse() * k_xFit_x.transpose();
}

VectorXf calcMeanVector(MatrixXf k_x_x, MatrixXf k_xFit_x, VectorXf y) {

    return k_xFit_x * k_x_x.inverse() * y;
}

extern "C"
void performGP(float *x, 
                float *y, 
                float *x_fit, 
                float *y_fit,
                float *y_error,
                int dim_data, 
                int dim_fit) {


    MatrixXf k_x_x = generateCovMatrix(x, x, dim_data, dim_data);
    MatrixXf k_xFit_x = generateCovMatrix(x_fit, x, dim_fit, dim_data);
    MatrixXf k_xFit_xFit = generateCovMatrix(x_fit, x_fit, dim_fit, dim_fit);

    Map<VectorXf> (y_error, dim_fit) =
        calcPosteriorCov(k_x_x, k_xFit_x, k_xFit_xFit).diagonal();

    Map<VectorXf> (y_fit, dim_fit) = 
        calcMeanVector(k_x_x, k_xFit_x, Map<VectorXf>(y, dim_data));
}