original = importdata("emission.txt");
measurement = importdata("measurement.txt");
variance = importdata("variance.txt");
r_coord = importdata("r_coordinates_reduced.txt");
z_coord = importdata("z_coordinates_reduced.txt");
transfer_mat = importdata("transfermatrix_volume_reduced.txt");
emission_reshape = importdata("emission_reshape.txt");

Sigma_D = diag(variance);
Sigma_E = zeros(966);

for i1 = 1:23
    for j1 = 1:42
        
        index1 = (j1-1)*length(r_coord) + i1;

        for i2 = 1:23
            for j2 = 1:42
                
                index2 = (j2-1)*length(r_coord) + i2;
                sqdist = (r_coord(i1) - r_coord(i2))^2 + (z_coord(j1) - z_coord(j2))^2;
                Sigma_E(index1, index2) = 400000^2 * exp(-sqdist/(2*0.13^2));

            end
        end  

    end
end    
tic
Sigma_E_inv = inv(Sigma_E + 0.001*diag(diag(Sigma_E)));
Sigma_D_inv = inv(Sigma_D);

Sigma_post_inv = transfer_mat' * Sigma_D_inv * transfer_mat + Sigma_E_inv;
Sigma_post = inv(Sigma_post_inv);

mu = Sigma_post * transfer_mat' * Sigma_D_inv * measurement;
mu_img = reshape(mu, [23 42]);
toc
%image(original, 'CDataMapping','scaled')
%figure;
%L = chol(Sigma_E + diag(diag(Sigma_E))*0.001 );
% tic
% Sigma_post_inv = (transfer_mat.' * inv(Sigma_D) * transfer_mat + inv(Sigma_E + 0.001*diag(diag(Sigma_E))));
% 
% L = chol(Sigma_post_inv);
% mu = L\L'\(transfer_mat.'*inv(Sigma_D)*measurement);
% 
% %mu = inv(transfer_mat.' * inv(Sigma_D) * transfer_mat + inv(Sigma_E + 0.001*diag(diag(Sigma_E))))*transfer_mat.'*inv(Sigma_D)*measurement;
% mu_img = reshape(mu, [23 42]);
% toc
% 
image(mu_img.', 'CDataMapping','scaled')