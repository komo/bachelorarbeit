from pathlib import Path
import numpy as np
import matplotlib as mpl
from src.metrics import *
import matplotlib.pyplot as plt
import src.plot_helper as plot_helper
from src.gp_inference import performGP, neg_log_likelihood, length_scale
from src.forward_model import get_training_emission, get_measurement
from src.optimize import optimize_params

#params = np.array([ 9.72371037e+05,  1.06096706e-01,  2.51460097e-01, -4.24859984e-01, -8.96749102e+00])
#params = np.array([5.10986068e+07, 1.31309929e-01, 8.13060134e-02, 4.61984566e+01, -7.48306556e+01])#43
#params = np.array([360000, 0.1143, 0.25, -0.6244, -9.89])#66
#params = np.array([472000, 0.04])#, 0.05, -0.02, -57.29])
#params = np.loadtxt("shitty_params.txt")
#params = np.array([260000, 1.84265978e-01])


transfer_mat = "volume"
cov_func = "rbf_sigmoid_z"

data_path = Path(__file__).parent / "data"
emission_dir =  data_path / "Input_emiss/"

mask = np.loadtxt(data_path / "mask.txt")

original = get_training_emission(3)
data = get_measurement(original, transfer_mat)

params = optimize_params(data, [500000, 0.07, 0.07, -0.55, -5], transfer_mat, "rbf_sigmoid_z")

fit, cov = performGP(data, params, format="image", cov_func=cov_func, transfer_mat=transfer_mat)
#print(fit[1].shape)

print(neg_log_likelihood(data, params, cov_func=cov_func))
# print(ssim(original, fit, data_range=original.max()-original.min()))
# print(psnr(original, fit, data_range=original.max()-original.min()))
# print(nrmse(original, fit))
# print(rdte(original, fit))
#fig, (ax1, ax2, ax3, ax4) = plt.subplots(1,4, figsize=(14,4))

# scales = np.zeros((42,23))
# z_coords = np.loadtxt("data/z_coordinates_reduced.txt")
# scales_z = np.array([length_scale(z, params) for z in z_coords])
# print(z_coords.shape)

# for i in range(23):
#     scales[:,i]= scales_z


# plot_helper.plot_emission(original, fig, ax1, title="Original")
# plot_helper.plot_emission(fit, fig, ax2, title="Fit", prevent_negative=True, limits=[original.min(), original.max()])
# plot_helper.plot_emission(scales, fig, ax3, title="length scale", interpol=None)
# plot_helper.plot_emission(np.sqrt(cov), fig, ax4, title="std", interpol=None)
# plt.tight_layout()

titles = plot_helper.title_generator(66, params)
plot_helper.compare_emissions([original, fit], titles=titles, cmap="viridis", adapt_colorscale=True)
# plot_helper.compare_emissions([original, fit], titles=["test1", "magma"], cmap="magma")
# plot_helper.compare_emissions([original, fit], titles=["test1", "inferno"], cmap="inferno")
# plot_helper.compare_emissions([original, fit], titles=["test1", "plasma"], cmap="plasma")
#plot_helper.compare_emissions([original, fit], titles=titles)

#plot_helper.plot_saver("NN_66_for_thesis.png")
#plot_helper.plot_saver("five_row_test.png", dpi=300)

# mask = np.loadtxt("data/mask.txt")
# r_coord = np.loadtxt("data/r_coordinates_reduced.txt")
# z_coord = np.loadtxt("data/z_coordinates_reduced.txt")

# fig, ax = plt.subplots(1, 1, figsize=(6,3.5))

# ax.set_title("Emission")
# from src.error_model import get_std
# error = get_std(data)
# ax.contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors="white")
# ax.imshow(original/1000000, interpolation="spline36",
#                 extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]])
# ax.set_ylabel("z in m")
# ax.set_xlabel("r in m")
# plot_helper.plot_saver("test2.png")

#plt.plot(data)



plt.show()