from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import src.plot_helper as plot_helper
from src.gp_inference import performGP, neg_log_likelihood
from src.optimize import optimize_params
from src.forward_model import get_measurement, get_training_emission
from scipy.signal import butter, sosfilt, sosfreqz, sosfilt_zi, sosfiltfilt
import time
from src.metrics import *


def axis_label(ax, label, pos, fontsize=12):
    limits = ax.get_xlim()[1]*pos[0], ax.get_ylim()[1]*pos[1]
    ax.annotate(label, xy=limits, fontsize=fontsize, fontweight='bold')

def butter_highpass(highcut, fs, order):
    nyq=0.5+fs
    high = highcut/nyq
    sos = butter(order, high, analog=False, btype="high", output='sos' )
    return sos

def butter_highpass_filter(data, highcut, fs, order=3):
    sos = butter_highpass(highcut, fs, order=order)
    return sosfiltfilt(sos, data)

def filter_data(data, cutoff=30, samplingrate=10000):
    highpass_data = butter_highpass_filter(data, cutoff, samplingrate, order=3)
    lowpass_data = data - highpass_data
    return highpass_data, lowpass_data

realdata = np.loadtxt("data/realtime_test/36354_0_9_seconds.txt")
realdata[np.isnan(realdata)] = 0
lowpass_data  = np.zeros((realdata.shape[0], realdata.shape[1]))
for i in range(128):
    _, low = filter_data(realdata[:, i])
    lowpass_data[:,i] = low

every50ms = lowpass_data[::125]
print(every50ms.shape)



#params = optimize_params(lowpass_data[15000], np.array([500000, 0.1]), transfer_mat="volume", cov_func="simple_rbf", remove_zero_measurements=True)
fit, _ = performGP(lowpass_data[15000], np.array([470000, 0.13]), format="image", remove_zero_measurements=True)


#print(params)
#plt.figure()
#plt.plot(range(1,129), lowpass_data[15000], label="low-pass")
# plt.plot(range(1,129),lowpass_data[15000], label="low-pass")
# plt.plot(range(1,129),, label="forward model")
#plt.plot(get_measurement(fit2, transfer_mat="volume"))
#plt.plot(get_measurement(fit, transfer_mat="volume"))
# plt.legend()

interpolated = lowpass_data[15000].copy()
for i in range(interpolated.size):
    if interpolated[i] <= 0:
        interpolated[i] = 0.5*(interpolated[i-1] + interpolated[i+1])

#params = optimize_params(interpolated, np.array([500000, 0.1]), transfer_mat="volume", cov_func="simple_rbf", remove_zero_measurements=True)
fit2, _ = performGP(get_measurement(get_training_emission(67)), np.array([500000, 0.13]), format="image", remove_zero_measurements=False)

# plt.plot(range(1,129), measurement, label="interpolated low_pass")
# plt.plot(range(1,129), get_measurement(get_training_emission(67)), label="measurement from training sample")
# plt.legend()
# fig, (ax0, ax1, ax2) = plt.subplots(1,3)
# plot_helper.plot_emission(get_training_emission(67), fig, ax0, prevent_negative=False)
# plot_helper.plot_emission(fit, fig, ax1, prevent_negative=True)
# plot_helper.plot_emission(fit2, fig, ax2, prevent_negative=True)


# fig = plt.figure(figsize=(10,12))
# grid = plt.GridSpec(4,1, hspace=0.6)
# ax0 = fig.add_subplot(grid[0,:])
# ax1 = fig.add_subplot(grid[1,:])
# ax2 = fig.add_subplot(grid[2:4,:])

# ax0.plot(np.array(range(realdata.shape[0]))/2500, realdata/1000)
# ax1.plot(np.array(range(realdata.shape[0]))/2500, lowpass_data/1000)
# ax0.vlines(6, ax0.get_ylim()[0], ax0.get_ylim()[1], "r", "dotted")
# ax1.vlines(6, ax1.get_ylim()[0], ax1.get_ylim()[1], "r", "dotted")
# ax0.set_title("Bolometer signals for Shot 36354")
# ax0.set_ylabel("Signals in kW")
# ax0.set_xlabel("Time in s")
# ax1.set_title("Low-pass signals")
# ax1.set_ylabel("Signals in kW")
# ax1.set_xlabel("Time in s")

# ax2.plot(range(1,129), interpolated/1000, label="interpolated low-pass", color="tab:green")
# ax2.plot(range(1,129), get_measurement(get_training_emission(67))/1000, label="via forward model from\ntraining sample", color="tab:red")
# ax2.plot(range(1,129),lowpass_data[15000]/1000, label="low-pass", color="k", linestyle="--")
# ax2.set_xlabel("Channel")
# ax2.set_ylabel("Signal in kW")
# ax2.set_title("Signal at t=6 s")

# ax2.legend()

# axis_label(ax0, "A)", (0.0, 0.72))
# axis_label(ax1, "B)", (0.0, 0.72))
# axis_label(ax2, "C)", (0.0, 0.9))

#plot_helper.plot_saver("signal_comparision_forwardmodel_real.png")

fig2, (ax3, ax4, ax5) = plt.subplots(1,3, figsize=(10, 4.8))



original = get_training_emission(67)
nrmse_test = nrmse(original, fit2)
psnr_test = psnr(original, fit2, data_range=original.max() - original.min())
rdte_test = rdte(original, fit2)
ssim_test = ssim(original, fit2, data_range=original.max() - original.min())

nrmse_real = nrmse(original, fit)
psnr_real = psnr(original, fit, data_range=original.max() - original.min())
rdte_real = rdte(original, fit)
ssim_real = ssim(original, fit, data_range=original.max() - original.min())

title =("Shot {} at t={}s\n" + 
            "Forward model:  NRMSE: {};  PSNR: {};  RDTE: {};  SSIM: {};\n" + 
            "With real data:   NRMSE: {};    PSNR: {};  RDTE: {};    SSIM: {};"
            ).format(36354,  6, 
                round(nrmse_test, 3), round(psnr_test, 1), 
                round(rdte_test, 4), round(ssim_test, 2), 
                round(nrmse_real, 3), round(psnr_real, 1), 
                round(rdte_real, 4), round(ssim_real, 2))

fig2.suptitle(title)

title1 = r"$\bf{A)}$" + " training sample"
title2 = r"$\bf{B)}$" + " reconstruction\nvia forward model"
title3 = r"$\bf{C)}$" + " tomogram from\noriginal data"


plot_helper.plot_emission(original, fig2, ax3, prevent_negative=False, title=title1)
plot_helper.plot_emission(fit2, fig2, ax4, limits=(0, original.max()), title=title2)
plot_helper.plot_emission(fit, fig2, ax5, limits=(0, original.max()), title=title3)

ax4.set_ylabel("")
ax5.set_ylabel("")
ax3.set_xlabel("")
ax5.set_xlabel("")

plt.tight_layout(rect=[0,0,1,0.85])

plot_helper.plot_saver("real_data_tomogram.png")

# length_scales = np.linspace(0.01, 0.3, 150)
# lls = []
# lls2 = []
# for l in length_scales:
#     lls.append(-neg_log_likelihood(lowpass_data[15000], np.array([params[0], l]), "volume", "simple_rbf", True))
#     lls2.append(-neg_log_likelihood(np.abs(lowpass_data[15000] - get_measurement(fit2)), np.array([params2[0], l]), "volume", "simple_rbf", True))


# plt.figure()
# plt.plot(length_scales, lls, label="first iteration")
# plt.plot(length_scales, lls2, label="2nd iteration with prior from 1st iteration")
# plt.title("last term")
# # plt.figure("real data")30
# plt.plot(realdata)
# plt.figure("low pass")
# plt.plot(lowpass_data)

plt.show()
# plt.figure()
# plt.plot(get_measurement(fit))
# plt.show()
# t3 = time.process_time()
# for i, data in enumerate(every250ms):
#     t1 = time.process_time()
#     performGP(data, np.array([10000000, 0.1]))
#     t2 = time.process_time()
#     #print(t2-t1)

# t4 = time.process_time()
# print("total time in seconds: ", t4-t3)
