import numpy as np
import matplotlib.pyplot as plt
from src.forward_model import get_measurement, get_training_emission
from src.error_model import sample_noise, get_std
from src.gp_inference import performGP
import src.plot_helper as plot_helper

params = np.array([2588380, 0.0788])

data = np.loadtxt("ForwardModel/results/measurement/NN_ 43_volume.txt")

sample1 = sample_noise(data)
sample2 = sample_noise(data)
sample3 = sample_noise(data)

fit_clean, cov_clean = performGP(data, params, format="image")
fit_s1, _ = performGP(sample1, params, format="image")
fit_s2, _ = performGP(sample2, params, format="image")
fit_s3, _ = performGP(sample3, params, format="image")

fig, axes = plt.subplots(2,3)

plot_helper.plot_emission(get_training_emission(43), fig, axes[0][0], title="Original")
plot_helper.plot_emission(fit_clean, fig, axes[0][1], title="Clean measurement")
plot_helper.plot_emission(np.sqrt(cov_clean), fig, axes[0][2], title="Std clean measurement")
plot_helper.plot_emission(fit_s1, fig, axes[1][0], title="Sample1")
plot_helper.plot_emission(fit_s2, fig, axes[1][1], title="Sample2")
plot_helper.plot_emission(fit_s3, fig, axes[1][2], title="Sample3")

plt.figure()

plt.errorbar(range(1,129), data, yerr = get_std(data), fmt=".k",
                     label="input measurements")
plt.scatter(range(1,129), sample1, color="orange",  marker="x", label="sample1")
plt.scatter(range(1,129), sample2, color="lime",  marker="x", label="sample2")
plt.scatter(range(1,129), sample3, color="fuchsia", marker="x", label="sample3")

plt.legend()


plt.show()