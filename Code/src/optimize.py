"""Optimize hyperparameters for given measurement"""

from scipy.optimize import minimize
from gp_inference import neg_log_likelihood


def neg_log_like(data, transfer_mat, cov_func_switch, remove_zero_measurements):
    """Returns function with just params as input so that it can be optimized"""
    def nll(params):
        res = neg_log_likelihood(   data, 
                                    params, 
                                    transfer_mat, 
                                    cov_func_switch, 
                                    remove_zero_measurements=remove_zero_measurements)
        #print(res)
        return res
    return nll


def optimize_params(measurement, 
                    params_initial, 
                    transfer_mat = "volume", 
                    cov_func = "simple_rbf", 
                    remove_zero_measurements = False):
    """
    Return numpy.ndarray with best parameters for given measurement

    Parameters
    ----------
    measurement : numpy.ndarray
    params_initial : numpy.ndarray
        initial guess for hyperparameters  
    transfer_mat : string
        type of transfermatrix ("line" or "volume"(default))
    cov_func : string
        type of covariance function ("simple_rbf"(default) or "rbf_sigmoid_z")
    remove_zero_measurements : boolean
        set transfermatrix for channels with zero/negative signal to zero

    Returns
    -------
    numpy.ndarray
        best hyperparameters 
    """

    opt = minimize(fun=neg_log_like(measurement, transfer_mat, cov_func, remove_zero_measurements), 
           x0=params_initial, method='Nelder-Mead', options={"xatol":100,  "fatol":0.1, "disp": True, "maxiter":350})
    print(opt.x)
    return opt.x

