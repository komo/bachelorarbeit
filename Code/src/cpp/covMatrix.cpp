// Functions for creating the covariance matrix and for calculating 
// the covariance between points for the chosen covariance function

#include "Eigen/Dense"
#include <math.h>
#include <iostream>
#include <chrono>

using namespace std;
using namespace Eigen;


// calculate condition number of matrix; use only for debugging numerical issues
double condition(MatrixXd matrix) {
    JacobiSVD<MatrixXd> svd1(matrix);
    return svd1.singularValues()(0) / 
            svd1.singularValues()(svd1.singularValues().size()-1);
}
    

// invert matrix via modified cholesky decomposition 
// this takes by far the most time. Better to lock for ways to circumvent 
// explicit inversion
MatrixXd invertMat(MatrixXd matrix) {
    MatrixXd unity = MatrixXd::Identity(matrix.rows(), matrix.cols());
    return matrix.ldlt().solve(unity);
}


//length scale for non stationary covariance function
double lengthScaleSigmoid(  double z_coord, 
                            double y_min, 
                            double y_offset, 
                            double x_offset, 
                            double abruptness   ) {
                                
    return y_min + y_offset / (1 + exp( -abruptness*(z_coord + x_offset)));
}


// simple stationary squared exponential covariance function
double covFunc1(double *pt1, double *pt2, int dim_pt, double *params) {

    double sqdist = pow(pt1[0]-pt2[0], 2) + pow(pt1[1]-pt2[1], 2);
    return pow(params[0], 2) * exp( -sqdist/(2 * pow(params[1], 2)) );
}


// non stationary convariance function described in section 4.3
double covFunc2(double *pt1, double *pt2, int dim_pt, double *params) {
    
    double l1 = lengthScaleSigmoid(pt1[1], params[1], params[2], 
                                        params[3], params[4]);
    double l2 = lengthScaleSigmoid(pt2[1], params[1], params[2], 
                                        params[3], params[4]);

    double sqdist = pow(pt1[0]-pt2[0], 2) + pow(pt1[1]-pt2[1], 2);

    return pow(params[0], 2) * exp( -sqdist/( ( l1*l1 + l2*l2 ) ));

}


// return value for chosen covariance function
double covarianceFunction(  double *pt1, 
                            double *pt2, 
                            int dim_pt, 
                            double *params, 
                            int func_switch) {
    double result = 0;
    switch(func_switch) {

        case 1:
            result = covFunc2(pt1, pt2, dim_pt, params);
        break;
        default:
            result = covFunc1(pt1, pt2, dim_pt, params);

    }
    return result;
}


// create covariance matrix by iterating over all pixels/coordinates
MatrixXd generateCovMatrix(VectorXd vector1,
                            VectorXd vector2, 
                            double *params,
                            int covFuncSwitch) {
    
    int dim = vector1.size()*vector2.size();
    MatrixXd cov_matrix(dim, dim);

    double pt1[2];
    double pt2[2];
    int index1 = 0;
    int index2 = 0;

    for (int i1 = 0; i1 < vector1.size(); i1++) {

        for (int j1 = 0; j1 < vector2.size(); j1++) {

            pt1[0] = vector1[i1];
            pt1[1] = vector2[j1];
            index1 = j1 * vector1.size() + i1;

            for (int i2 = 0; i2 < vector1.size(); i2++) {
                for (int j2 = 0; j2 < vector2.size(); j2++) {

                    pt2[0] = vector1[i2];
                    pt2[1] = vector2[j2];
                    index2 = j2 * vector1.size() + i2;
                
                    cov_matrix(index1, index2) = 
                        covarianceFunction(pt1, pt2, 2, params, covFuncSwitch);
                }
            }
        }
    }
    
    // add small fraction of sigma_E to diagonal to decrease condition number 
    // by some orders of magnitude
    return cov_matrix + 0.002*params[0]*MatrixXd::Identity(dim, dim);
}