// The functions in this file are used to load data like the transfermatrix 
// or the coordinates for the vessel directly in C++

#include "Eigen/Dense"
#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;
using namespace Eigen;


int numberOfLines(string filename) {
    int lines = 0;
    string line;
    ifstream file(filename);

    while(getline(file, line)) {
        if(line != "") ++lines; //skip empty lines
    }

    return lines;
}

int numberOfColumns(string filename, char delimiter,  int skiprows) {
    int cols = 0;
    int rows = 0;
    string line;
    ifstream file(filename);

    while(getline(file, line)) {
        if(rows >= skiprows) {
            cols = count(line.begin(), line.end(), delimiter);
            // add column for last element if last char is not delimiter
            if (line.back() != delimiter) ++cols; 
            break;
        }
        ++rows;        
    }
    return cols;
}

VectorXd loadVector(string filename) {

    int size = numberOfLines(filename);
    VectorXd vector(size);
    ifstream file(filename);
    string line;

    int index = 0;
    while(getline(file, line)) {
        vector[index] = stod(line);
        ++index;
    }
    return vector;
}

MatrixXd loadMatrix(string filename, char delimiter,  int skiprows) {
    int rows = numberOfLines(filename);
    int cols = numberOfColumns(filename, delimiter, skiprows);
    MatrixXd matrix(rows, cols);

    string element;
    ifstream file(filename);

    for (int row = 0; row < rows; row++) {
        for (int col = 0; col < cols-1 ; col++) {
            getline(file, element, delimiter);
            matrix(row, col) = stod(element);
        }
        if (element.back() == delimiter) {
            getline(file, element, delimiter);
        } else {
            getline(file, element);
        }
        matrix(row, cols-1) = stod(element);
    }
    return matrix;
}

// int main() {
//     MatrixXd mat = loadMatrix("../data/transfermatrix_reduced.dat", ' ', 0);
//     cout << numberOfLines("../data/transfermatrix_reduced.dat") << "\n";
//     //cout << loadVector("../data/z_coord.txt") << "\n";
//     cout << loadMatrix("../data/transfermatrix_reduced.dat", ' ', 0).cols() << "\n";
// }