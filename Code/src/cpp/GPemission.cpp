// Here the magic happens; functions for calculating the posterior mean 
// and covariance matrix and the negative marginal log likelyhood

#include "Eigen/Dense"
#include <iostream>
#include "loaddata.cpp"
#include "covMatrix.cpp"
#include <chrono>

#define TRANSFER_MATRIX_LINE_DIR "data/transfermatrix_line_reduced.txt"
#define TRANSFER_MATRIX_VOLUME_DIR "data/transfermatrix_volume_reduced.txt"
#define R_COORDINATES_DIR "data/r_coordinates_reduced.txt"
#define Z_COORDINATES_DIR "data/z_coordinates_reduced.txt"

using namespace Eigen;

VectorXd rCoordinates = loadVector(R_COORDINATES_DIR);
VectorXd zCoordinates = loadVector(Z_COORDINATES_DIR);
MatrixXd transferMatLine = loadMatrix(TRANSFER_MATRIX_LINE_DIR, ' ', 0);
MatrixXd transferMatVol = loadMatrix(TRANSFER_MATRIX_VOLUME_DIR, ' ', 0);


// this is only used for some plots in the thesis
extern "C"
double lengthScale(double z, double *params) {
    return lengthScaleSigmoid(z, params[1], params[2], params[3], params[4]);
}


// set entries in tranfermatrix to 0 for channels where singal is <= 0
void setToZero(MatrixXd *matrix, VectorXd vector) {
    for (int i=0; i<vector.size(); i++) {
        if (vector[i]<=0) {
            (*matrix).row(i).setZero();
        }
    }
}

MatrixXd getTransferMatrix(int type) {
    if (type == 0) {
        return transferMatLine;
    } else {
        return transferMatVol;
    }
}


// calculate posterior mean and covariance. The fuction has no return values 
// instead initialized arrays (pointer to numpy arrays) are given as arguments. 
// The results are then stored in them
extern "C"
void performGP( double *emission, 
                double *emission_variance,
                double *measurement, 
                double *error,
                double *params,
                int dimMeasurement,
                int transferMatrixType,
                int covFuncSwitch,
                bool removeZeroMeasurements   ) {
    
    // Matplotlib sometimes modifies the systemwide setting for the decimal 
    // delimiter, this can result in errors when the data was loaded from the files
    setlocale(LC_NUMERIC, "C");

    MatrixXd transferMat = getTransferMatrix(transferMatrixType); 

    // generate matrices from arguments
    VectorXd d_m = Map<VectorXd>(measurement, dimMeasurement);
    MatrixXd sigma_d = MatrixXd::Zero(dimMeasurement, dimMeasurement); 
    sigma_d.diagonal() =  Map<VectorXd>(error, dimMeasurement);

    if (removeZeroMeasurements) {
        setToZero(&transferMat, d_m);
    }

    //Generate kernel matrix
    MatrixXd sigma_E = generateCovMatrix(rCoordinates, zCoordinates, params, 
                                            covFuncSwitch);

    //auto t1 = std::chrono::high_resolution_clock::now();

    MatrixXd sigma_E_inv = invertMat(sigma_E);
    //MatrixXd sigma_E_inv = (sigma_E).inverse();

    
    MatrixXd sigma_d_inv = sigma_d.inverse();

    MatrixXd posteriorCovInv = (transferMat.transpose() * sigma_d_inv * 
                                    transferMat + sigma_E_inv);

    MatrixXd posteriorCov = invertMat(posteriorCovInv);
    //MatrixXd posteriorCov = (posteriorCovInv).inverse();

    VectorXd mean = (posteriorCov * transferMat.transpose() * 
                        sigma_d_inv * d_m);

    //auto t2 = std::chrono::high_resolution_clock::now();
    //auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
    //cout << "duration in ms: " << duration/1000 << "\n";
    

    Map<VectorXd> (emission, rCoordinates.size()*zCoordinates.size()) = mean;
    Map<VectorXd> (emission_variance, rCoordinates.size()*zCoordinates.size()) = 
                                                        posteriorCov.diagonal();
}

// return negative marginal log likelyhood
extern "C"
double negLogMargLike(  double *measurement,
                        double *error,
                        double *params,
                        int dimMeasurement,
                        int transferMatrixType,
                        int covFuncSwitch ,
                        bool removeZeroMeasurements  )       {
    
    // see perform GP
    setlocale(LC_NUMERIC, "C");


    MatrixXd transferMat = getTransferMatrix(transferMatrixType); 

    // generate matrices from arguments
    VectorXd d_m = Map<VectorXd>(measurement, dimMeasurement);
    
    MatrixXd sigma_d = MatrixXd::Zero(dimMeasurement, dimMeasurement); 
    sigma_d.diagonal() =  Map<VectorXd>(error, dimMeasurement);

    if (removeZeroMeasurements) {
        setToZero(&transferMat, d_m);
    }

    MatrixXd sigma_E = generateCovMatrix(rCoordinates, zCoordinates, 
                                            params, covFuncSwitch);

    MatrixXd intermediate = transferMat * sigma_E * transferMat.transpose() 
                        +  sigma_d;

    // add small diagonal to matrix to reduce condition number, sadly
    // this does not resolve all problems for the non stationary convariance 
    // function. Sometimes the condition number still gets very large 
    // and completely wrong values are calculated as a result
    double epsilon = 0.005 * intermediate.diagonal().minCoeff();
    intermediate = intermediate + epsilon*MatrixXd::Identity(dimMeasurement, dimMeasurement);

    // calculate logarithm of determinant via cholesky decomposition
    MatrixXd cholesky = intermediate.llt().matrixL();
    double log_determinant = cholesky.diagonal().array().log().sum();

    //cout << (intermediate).diagonal()  << "\n";
    // cout << "cond covPoints: " << cond1 << ";  params: " << params[0]
    //      << ", " << params[1] << endl;
    //cout << cholesky.diagonal() << endl;
    //cout << log_chol << "; " << 0.5 * d_m.transpose() * intermediate.ldlt().solve(d_m) << endl;

    return  0.5 * d_m.transpose() * intermediate.ldlt().solve(d_m) +
                    log_determinant + 0.5* dimMeasurement * log(2*M_PI);
}