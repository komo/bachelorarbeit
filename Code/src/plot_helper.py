import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.absolute()))
import numpy as np 
import matplotlib.pyplot as plt
from pathlib import Path
from mpl_toolkits.axes_grid1 import make_axes_locatable, ImageGrid
from metrics import *

data_dir = Path(__file__).parents[1] / "data"

mask = np.loadtxt("data/mask.txt")
r_coord = np.loadtxt("data/r_coordinates_reduced.txt")
z_coord = np.loadtxt("data/z_coordinates_reduced.txt")


def map_training_to_real_shot(file_number):
    """Returns shotnumber and time of the training emission"""
    shot_list = np.genfromtxt(data_dir / "shot_list_training.txt",skip_header=1)
    return int(shot_list[file_number - 1 , 0]), shot_list[file_number - 1, 2]


def plot_saver(path, dpi=350):
    plt.savefig(path, bbox_inches='tight', dpi=dpi, transparent=True)


def plot_emission(  emission, 
                    fig, 
                    ax, 
                    title="", 
                    interpol="spline36", 
                    prevent_negative = False, 
                    limits=None, 
                    cmap="viridis"):
    """
    Takes emission as 2D input and creates an imshow with the right axis
    label and ticks for given matplotlib figure and axis. Optionally
    a title and a method for interpolation (default: spline36) can be specified.
    prevent negative sets the minimum of the colorscale to 0. Alternatively
    both the minimum and maximum can be specified explicitely.
    The colormap can also be set.
    """

    ax.set_xlabel("r in m")
    ax.set_ylabel("z in m")
    ax.set_title(title)
    if prevent_negative:
        emission[emission<0] = 0
    if not limits:
        limits=[emission.min(), emission.max()]
    if np.isnan(emission).any():
        limits[1] = np.nan

    ax.contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors='white')
    img = ax.imshow(emission/1000000, interpolation=interpol, vmin=limits[0]/1000000, vmax=limits[1]/1000000,
                extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]], cmap=cmap)

    fig.colorbar(img, ax=ax, label="Emission in MW/m³")


def compare_emissions(  emissions, 
                        titles,
                        interpol="spline36", 
                        cmap="viridis", 
                        contour_color="white", 
                        adapt_colorscale=True,
                        figsize=(6.8,4.2)):

    wspace = 1 * (not adapt_colorscale)

    fig, axes = plt.subplots(1, len(emissions), sharey=adapt_colorscale, 
                    gridspec_kw = {'wspace':wspace, 'hspace':0}, 
                    figsize=figsize, 
                    constrained_layout=False)

    fig.suptitle(titles[0], fontsize=14)

    images = []

    for i, ax in enumerate(axes):

        vmin = 0
        vmax = emissions[0].max()/1000
        if not adapt_colorscale:
            vmin = emissions[i].min()/1000
            vmax = emissions[i].max()/1000

        ax.contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors=contour_color)
        img = ax.imshow(emissions[i]/1000, interpolation=interpol, vmin=vmin, vmax=vmax,
                extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]], cmap=cmap)
        images.append(img)
        ax.set_title(titles[i+1])
        ax.set_xlabel("r in m")
        if i==0 or not adapt_colorscale: ax.set_ylabel("z in m")
        #if i>0: ax.set_yticks([])
    
    if adapt_colorscale:
        divider = make_axes_locatable(axes[-1])
        cax = divider.append_axes("right", size="8%", pad=0.05)
        fig.colorbar(images[0], cax=cax, label="Emission in kW/m³")
    else:
        for i, ax in enumerate(axes):
            divider = make_axes_locatable(ax)
            cax = divider.append_axes("right", size="8%", pad=0.05)
            fig.colorbar(images[i], cax=cax, label="Emission in kW/m³")

        plt.tight_layout(rect=[0,0,1,0.1])
    return fig


def line_volume_fit_comparison(file_number, emissions, covs, opt_vol, opt_line):
    
    shot, time = map_training_to_real_shot(file_number)

    nrmse_line = nrmse(emissions[0], emissions[2])
    psnr_line = psnr(emissions[0], emissions[2], data_range=emissions[0].max() - emissions[0].min())
    rdte_line = rdte(emissions[0], emissions[2])
    ssim_line = ssim(emissions[0], emissions[2], data_range=emissions[0].max() - emissions[0].min())

    nrmse_vol = nrmse(emissions[0], emissions[1])
    psnr_vol = psnr(emissions[0], emissions[1], data_range=emissions[0].max() - emissions[0].min())
    rdte_vol = rdte(emissions[0], emissions[1])
    ssim_vol = ssim(emissions[0], emissions[1], data_range=emissions[0].max() - emissions[0].min())
    
    title =("Shot {} at t={}s\n" + 
            "Volume matrix:   " + r"$\sigma_E$: " + "{} kW;  l: {} cm;  NRMSE: {};  PSNR: {};  RDTE: {};  SSIM: {};\n" + 
            "Line matrix:     " + r"$\sigma_E$: " + "{} kW;  l: {} cm;   NRMSE: {};  PSNR: {};  RDTE: {};  SSIM: {};"
            ).format(int(shot),  time, 
                int(np.abs(opt_vol[0])/1000), int(opt_vol[1]*100), 
                round(nrmse_vol, 3), round(psnr_vol, 1), 
                round(rdte_vol, 4), round(ssim_vol, 3), 
                int(np.abs(opt_line[0])/1000), int(opt_line[1]*100),
                round(nrmse_line, 3), round(psnr_line, 1), 
                round(rdte_line, 4), round(ssim_line, 3))
    
    plt.rcParams["mpl_toolkits.legacy_colorbar"] = False
    
    fig = plt.figure(figsize=(10,4.2))
    fig.text(0.5, 0.01, "r in m", ha="center", fontsize=11)
    fig.suptitle(title)
    grid_fit = ImageGrid(fig, [0.065,-0.015,0.45,0.9],
                     nrows_ncols=(1, 3),
                     axes_pad=0.04,
                     share_all=True,
                     label_mode="L",
                     cbar_location="right",
                     cbar_mode="single",)

    grid_cov = ImageGrid(fig, [0.64,-0.015,0.3,0.9],
                     nrows_ncols=(1, 2),
                     axes_pad=0.04,
                     share_all=True,
                     label_mode="L",
                     cbar_location="right",
                     cbar_mode="single",)
    
    emission_titles = ["original", "prediction volume", "prediction line"]
    cov_titles = ["std volume", "std line"]

    grid_fit[0].set_ylabel("z in m", fontsize=11)
    for i, ax in enumerate(grid_fit):
        ax.set_title(emission_titles[i], fontsize=11)
        ax.contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors="white")
        img = ax.imshow(emissions[i]/1000000, interpolation="spline36", vmin=0, vmax=emissions[0].max()/1000000,
                extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]])
    cb = grid_fit.cbar_axes[0].colorbar(img, label="Emission in MW/m³")
    cb.set_label("Emission in MW/m³", size=11)

    cov_max = np.sqrt(max(covs[0].max(), covs[1].max()))/1000000
    for i, ax in enumerate(grid_cov):
        ax.set_title(cov_titles[i], fontsize=11)
        ax.contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors="white")
        img = ax.imshow(np.sqrt(covs[i])/1000000, interpolation="spline36", vmin=0, vmax=cov_max,
                extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]])
    cb = grid_cov.cbar_axes[0].colorbar(img)
    cb.set_label("std in MW/m³", labelpad=7, size=11)


def compare_stationary_nonstationary(file_number, emissions, length_scales):
    
    shot, time = map_training_to_real_shot(file_number)

    nrmse_stat = nrmse(emissions[0], emissions[1])
    psnr_stat = psnr(emissions[0], emissions[1], data_range=emissions[0].max() - emissions[0].min())
    rdte_stat = rdte(emissions[0], emissions[1])
    ssim_stat = ssim(emissions[0], emissions[1], data_range=emissions[0].max() - emissions[0].min())

    nrmse_non = nrmse(emissions[0], emissions[2])
    psnr_non = psnr(emissions[0], emissions[2], data_range=emissions[0].max() - emissions[0].min())
    rdte_non = rdte(emissions[0], emissions[2])
    ssim_non = ssim(emissions[0], emissions[2], data_range=emissions[0].max() - emissions[0].min())
    
    title =("Shot {} at t={}s\n" + 
            "Stationary:         NRMSE: {};  PSNR: {};  RDTE: {};  SSIM: {};\n" + 
            "Non stationary:  NRMSE: {};  PSNR: {};  RDTE: {};  SSIM: {};"
            ).format(int(shot),  time, 
                round(nrmse_stat, 3), round(psnr_stat, 1), 
                round(rdte_stat, 4), round(ssim_stat, 2), 
                round(nrmse_non, 3), round(psnr_non, 1), 
                round(rdte_non, 4), round(ssim_non, 2))
    
    plt.rcParams["mpl_toolkits.legacy_colorbar"] = False
    
    fig = plt.figure(figsize=(10,5))
    fig.text(0.5, 0.01, "r in m", ha="center")
    fig.suptitle(title)
    grid_fit = ImageGrid(fig, [0.065,-0.015,0.58,0.9],
                     nrows_ncols=(1, 3),
                     axes_pad=0.04,
                     share_all=True,
                     label_mode="L",
                     cbar_location="right",
                     cbar_mode="single",)

    grid_len = ImageGrid(fig, [0.75,-0.015,0.2,0.9],
                     nrows_ncols=(1, 1),
                     axes_pad=0.04,
                     share_all=True,
                     label_mode="L",
                     cbar_location="right",
                     cbar_mode="single",)
    
    emission_titles = ["original", "stationary", "non stationary"]
    
    grid_fit[0].set_ylabel("z in m")
    for i, ax in enumerate(grid_fit):
        ax.set_title(emission_titles[i])
        ax.contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors="white")
        img = ax.imshow(emissions[i]/1000000, interpolation="spline36", vmin=0, vmax=emissions[0].max()/1000000,
                extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]])
    cb = grid_fit.cbar_axes[0].colorbar(img, label="Emission in MW/m³")
    cb.set_label("Emission in MW/m³")

    length_scales[np.logical_not(mask)] = length_scales.min()

    grid_len[0].set_title(r"local length scale")
    grid_len[0].contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors="white")
    img = grid_len[0].imshow(length_scales*100,
                extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]])
    cb = grid_len.cbar_axes[0].colorbar(img)
    cb.set_label("Length scale in cm")


def compare_several(emissions, emission_titles, figsize=(9.5,6.7), params1=None, params2=None):
    plt.rcParams["mpl_toolkits.legacy_colorbar"] = False
    shot, time = map_training_to_real_shot(8)

    nrmse_unscaled = nrmse(emissions[0], emissions[1])
    psnr_unscaled = psnr(emissions[0], emissions[1], data_range=emissions[0].max() - emissions[0].min())
    rdte_unscaled = rdte(emissions[0], emissions[1])
    ssim_unscaled = ssim(emissions[0], emissions[1], data_range=emissions[0].max() - emissions[0].min())

    nrmse_scaled = nrmse(emissions[0], emissions[2])
    psnr_scaled = psnr(emissions[0], emissions[2], data_range=emissions[0].max() - emissions[0].min())
    rdte_scaled = rdte(emissions[0], emissions[2])
    ssim_scaled = ssim(emissions[0], emissions[2], data_range=emissions[0].max() - emissions[0].min())

    title = ("Shot {} at t={}s\n" +
            "Unscaled:  " + r"$\sigma_E$: " + "{} kW;  l: {} cm;  NRMSE: {};  PSNR: {};  RDTE: {};  SSIM: {};\n" + 
            "Scaled:    " + r"$\sigma_E$: " + "{} kW;  l: {} cm;  NRMSE: {};  PSNR: {};  RDTE: {};  SSIM: {};"
            ).format(int(shot),  time, 
                int(params1[0]/1000), int(params1[1]*100), 
                round(nrmse_unscaled, 3), round(psnr_unscaled, 1), 
                round(rdte_unscaled, 4), round(ssim_unscaled, 3), 
                int(params2[0]/1000), int(params2[1]*100),
                round(nrmse_scaled, 3), round(psnr_scaled, 1), 
                round(rdte_scaled, 4), round(ssim_scaled, 3))

    fig = plt.figure(figsize=figsize)
    fig.suptitle(title)

    grid = ImageGrid(fig, 111,
                     nrows_ncols=(1, len(emissions)),
                     axes_pad=0.04,
                     share_all=True,
                     label_mode="L",
                     cbar_location="right",
                     cbar_mode="single",)

    grid[0].set_ylabel("z in m")
    grid[1].set_xlabel("r in m")
    #fig.text(0.5,0.02, "r in m", ha="center")

    for i, ax in enumerate(grid):
        ax.set_title(emission_titles[i])
        ax.contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors="white")
        img = ax.imshow(emissions[i]/1000000, interpolation="spline36", vmin=0, vmax=emissions[0].max()/1000000,
                extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]])
        cb = grid.cbar_axes[0].colorbar(img)
        cb.set_label("Emission in MW/m³")


def title_generator(file_number, parameters, transfer_mat="volume", cov_function="simple_rbf"):
    shot, time = map_training_to_real_shot(file_number)
    fig_title = "Shot {} at t={}s;  ".format(int(shot), time) + \
                "$\sigma_E=$" + str(round(parameters[0]/1000, 2)) + "kW; " \
                + " l={}cm".format(round(parameters[1]*100, 1)) 

    original_title = "Original"
    fit_title = "Prediction"

    return [fig_title, original_title, fit_title]
