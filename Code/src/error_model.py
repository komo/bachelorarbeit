"""provides functions to get std, variance ... for given measurement"""

import numpy as np
from pathlib import Path

noise_analysis_path = Path(__file__).parents[1] / "NoiseAnalysis/results"

background_noise = np.loadtxt(noise_analysis_path / 
                "BackgroundNoise/background_noise_complete.txt")[:,0]
# set to 1 if background noise is 0 or cov_error wouldn't be invertible
# this is the case for broken bolometers where no signals were recorded
background_noise[np.isnan(background_noise)] = 10000
signal_dependence = np.loadtxt(noise_analysis_path / 
                "SignalDependentNoise/noise_dependence_complete.txt")[:,0]
# negative slopes are set to zero
signal_dependence[signal_dependence < 0] = 0


def get_variance(data):
    """Returns the variance of a given sensor measurement"""
    return background_noise**2 + signal_dependence * data


def get_std(data):
    """Returns the standart deviation of a given sensor measurement"""
    return np.sqrt(get_variance(data))


def sample_noise(data):
    """Samples from data with std. Negative Signals are set to 0"""
    std = get_std(data)
    res = np.random.normal(loc=data, scale=std)
    res[res < 0] = 0
    return res

