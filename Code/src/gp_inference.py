"""Wrapper for C++ funcions"""
import sys
from pathlib import Path
sys.path.append(str(Path(__file__).parent.absolute()))
import ctypes
import numpy as np
from error_model import get_variance, get_std

lib_path = Path(__file__).parents[1].absolute() / "lib"

lib = ctypes.cdll.LoadLibrary(lib_path / "GPemission.so")

"""Inputs for lib.lengthScale:
double  : z cordinate
double* : parameters of covariance function

Returns:
double  : local lengthscale

Only used to get plots for thesis
"""
lib.lengthScale.argtypes = [
    ctypes.c_double,
    np.ctypeslib.ndpointer(np.float64, flags='aligned, c_contiguous')
]
lib.lengthScale.restype = ctypes.c_double

"""Parameters for lib.performGP:
double* : pointer to array for most likely emission tomography
double* : pointer to array for posterior covariance diagonal
double* : pointer to array for measurement data
double* : pointer to array for measurement variance
double* : pointer to array containing parameters for covariance function
int     : length of measurement array
int     : type of transfermatrix
int     : type of covariance function
bool    : set transfermatrix for channels with zero/negative signal to zero

Note:   Funcion has no return values. 
        Instead given arrays for results are modified.
"""
lib.performGP.argtypes = [

    np.ctypeslib.ndpointer(np.float64, flags='aligned, c_contiguous'),
    np.ctypeslib.ndpointer(np.float64, flags='aligned, c_contiguous'),
    np.ctypeslib.ndpointer(np.float64, flags='aligned, c_contiguous'),
    np.ctypeslib.ndpointer(np.float64, flags='aligned, c_contiguous'),
    np.ctypeslib.ndpointer(np.float64, flags='aligned, c_contiguous'),
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_bool
]

"""Parameters for lib.negLogMargLike:
double* : pointer to array for measurement data
double* : pointer to array for measurement variance
double* : pointer to array containing parameters for covariance function
int     : length of measurement array
int     : type of transfermatrix
int     : type of covariance function
bool    : set transfermatrix for channels with zero/negative signal to zero

Returns:
float   : result of negative logarithmic marginal likelyhood   
"""
lib.negLogMargLike.argtypes = [

    np.ctypeslib.ndpointer(np.float64, flags='aligned, c_contiguous'),
    np.ctypeslib.ndpointer(np.float64, flags='aligned, c_contiguous'),
    np.ctypeslib.ndpointer(np.float64, flags='aligned, c_contiguous'),
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_int,
    ctypes.c_bool
]

lib.negLogMargLike.restype = ctypes.c_double

def performGP(  measurement, 
                params, 
                transfer_mat="volume", 
                cov_func="simple_rbf", 
                dim=(42,23),
                format="raw" ,
                remove_zero_measurements = False):
    """Get posterior mean (emission) and covariance for given measurement

    Parameters
    ----------
    measurement : numpy.ndarray
    params : numpy.ndarray
        Array containing the parameters for covariance function.
        Number of parameters depends on covariance funcion used.
    transfer_mat : string, optional
        specifies which transfer matrix is used. Options: "volume", "line"
    cov_func : string, optional
        specifies which covariance funcion is used.
    dim : tuple of int, optional
        size of tomography in pixels
    format : string, optional
        Default is "raw", results are returned as one dimensional vector.
        If set to "image". Result is returned as matrix with right dimensions 
        and multiplied with vessel shape.
    remove_zero_measurements : boolean
        set transfermatrix for channels with zero/negative signal to zero

    Returns
    -------
    fit : np.ndarray
        Most likely emission tomography for given data
    postCov : np.ndarray
        Diagonal elements (=variance) of posterior covariance matrix.
        Can be used to estimate error of tomography.
    """

    size = dim[0]*dim[1]
    fit = np.zeros((size))
    postCov = np.zeros((size))

    lib.performGP(  fit, 
                    postCov, 
                    measurement, 
                    get_variance(measurement), 
                    params, 
                    measurement.size, 
                    get_transfermat_id(transfer_mat), 
                    get_cov_func_id(cov_func),
                    remove_zero_measurements   )

    if format == "image":
        mask = np.loadtxt("data/mask.txt")
        fit = fit.reshape(dim[0], dim[1]) * mask
        postCov = postCov.reshape(dim[0], dim[1]) * mask
    elif format != "raw": 
        raise ValueError("{} is not a valid return format!".format(format))
    
    return fit, postCov

def neg_log_likelihood( measurement, 
                        params, 
                        transfer_mat="volume", 
                        cov_func="simple_rbf",
                        remove_zero_measurements=False):
    """Get negative logarithmic marginal likelihood.

    Parameters
    ----------
    measurement : numpy.ndarray
    params : numpy.ndarray
        Array containing the parameters for covariance function.
        Number of parameters depends on covariance funcion used.
    transfer_mat : string, optional
        specifies which transfer matrix is used. Options: "volume", "line"
    cov_func : string, optional
        specifies which covariance funcion is used.
    remove_zero_measurements : boolean
        set transfermatrix for channels with zero/negative signal to zero

    Returns
    -------
    double
        Negative logarithmic marginal likelihood for given data and parameters.
    """
    
    

    res = lib.negLogMargLike(   measurement, 
                                get_variance(measurement), 
                                params, 
                                measurement.size, 
                                get_transfermat_id(transfer_mat), 
                                get_cov_func_id(cov_func),
                                remove_zero_measurements   )
    
    return res


def length_scale(z, params):
    """Return lengthscale for given z coordinate and parameters"""
    return lib.lengthScale(z, params)


def get_cov_func_id(cov_func):
    """Maps cov_func (string) to right integer for C++"""
    if cov_func == "simple_rbf": return 0
    if cov_func == "rbf_sigmoid_z": return 1
    else: 
        msg = "Covariance function '{}' does not exist!".format(cov_func)
        raise ValueError(msg)


def get_transfermat_id(transfer_mat):
    """Maps transfer_mat (string) to right integer for C++"""
    if transfer_mat == "line": return 0
    if transfer_mat == "volume": return 1
    else: 
        msg = "Transfermatrix '{}' does not exist!".format(transfer_mat)
        raise ValueError(msg)
