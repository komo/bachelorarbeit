"""Getting measurements from emissions and loading training Emissions"""
import numpy as np
from pathlib import Path


data_dir = Path(__file__).parents[1] / "data"

# Original Data consists of 83x45 pixels. This is too large for GPs.
# Therefore we downsample both the transfermatrix and training_data
def downsample_2x(mat):
    """Downsampling of last two axes of given numpy array by factor 2"""
    orig_dtype = mat.dtype
    mat = mat.astype(float)
    mat = (mat[...,1::2,:] + mat[...,:-1:2,:])/2.
    mat = (mat[...,:,1::2] + mat[...,:,:-1:2])/2.
    return mat.astype(orig_dtype)


mask = np.loadtxt(data_dir / "mask.txt")
transfer_vol = np.loadtxt( data_dir / "transfermatrix_volume_reduced.txt")
transfer_line = np.loadtxt(data_dir / "transfermatrix_line_reduced.txt")


def get_measurement(emission_tomography, transfer_mat="volume"):
    """Get measurement data from emission data via transfermatrix

    Parameters
    ----------
    emission_tomography : numpy.ndarray
        data of emission tomography as two dimensional array
    transfer_mat : string
        specifies which transfermatrix is used.
        Valid arguments are "line" and "volume"
    
    Returns
    -------
    numpy.ndarray
        one dimensional array containing measurements for sensor 
        according to transfermatrix
    """

    transfer = transfer_vol
    if transfer_mat=="line": transfer = transfer_line
    shape = emission_tomography.shape
    emission_tomography = emission_tomography.reshape(shape[0]*shape[1])
    return transfer @ emission_tomography


def get_training_emission(file_number, file_name = "NN_ {}.txt"):
    """Read training data (emission tomograms) from file"""
    
    f = data_dir / "training_samples" / file_name.format(file_number)

    emission = np.zeros((85,47))
    emission[1:-1,1:-1] = np.genfromtxt(f, skip_header=1).reshape(83,45)[::-1]
    
    return downsample_2x(emission)*mask
