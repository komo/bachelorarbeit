"""Metrics for comparing training samples with predictions"""

import numpy as np
from skimage.metrics import structural_similarity as ssim
from skimage.metrics import peak_signal_noise_ratio as psnr

def nrmse(original, fit):
    error = np.linalg.norm(original-fit)
    norm = np.linalg.norm(original)
    return error/norm

def rdte(original, fit):
    return np.abs(1-(fit.sum()/original.sum()))