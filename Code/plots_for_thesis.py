from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from src.forward_model import get_training_emission, get_measurement
from src.plot_helper import plot_emission
from src.error_model import get_std
from src.gp_inference import performGP, length_scale
from src.optimize import optimize_params
from src.plot_helper import line_volume_fit_comparison, plot_saver, compare_several, compare_stationary_nonstationary, map_training_to_real_shot

def axis_label(ax, label, pos, fontsize=12):
    limits = ax.get_xlim()[1]*pos[0], ax.get_ylim()[1]*pos[1]
    ax.annotate(label, xy=limits, fontsize=fontsize, fontweight='bold')

# fig, (ax1, ax2) = plt.subplots(2,1, figsize=(9,5), sharex="col")

# ax2.set_ylabel("Measurement in kW")
# ax1.set_ylabel("Measurement in kW")
# ax2.set_xlabel("Channel")
# ax1.set_title("Measurements via forwardmodel for shot 36448 at t=4.35s")
# ax2.set_title("Measurements via forwardmodel for shot 33032 at t=6.0s")

# emission = get_training_emission(67)
# meas = get_measurement(emission)
# error = get_std(meas)
# ax1.errorbar(range(1,129), meas/1000, yerr = error/1000, fmt=".k", color="black", markersize=6, capsize=2, label="input measurements")

# emission = get_training_emission(8)
# meas = get_measurement(emission)
# error = get_std(meas)
# ax2.errorbar(range(1,129), meas/1000, yerr = error/1000, fmt=".k", color="black", markersize=6, capsize=2)

# axis_label(ax1, "A)", (0.0, 0.82))
# axis_label(ax2, "B)", (0.0, 0.7))

# plot_saver("measurements_comparison_good_bad.png")

# emission = get_training_emission(8)
# print(2000000/emission.max())
# emission_scaled = emission * 2000000/emission.max()
# meas = get_measurement(emission)
# meas_scaled = get_measurement(emission_scaled)
# params = optimize_params(meas, [400000, 0.15], "volume", "simple_rbf")
# params_scaled = optimize_params(meas_scaled, [400000, 0.15], "volume", "simple_rbf")
# fit, cov_vol = performGP(meas, params, transfer_mat="volume", cov_func="simple_rbf", format="image")
# fit_scaled, cov_vol = performGP(meas_scaled, params_scaled, transfer_mat="volume", cov_func="simple_rbf", format="image")
# fit_scaled = fit_scaled/(2000000/emission.max())

# compare_several([emission, fit, fit_scaled], ["original", r"$\bf{A)}$  prediction unscaled", r"$\bf{B)}$  prediction scaled"], params1=params, params2=params_scaled)
# plot_saver("effect_of_rescaling.png")
# plt.show()

# good_plots = [66, 67,43]
# bad_plots = [8,14,4]

# for file_number in [4]:
#     original = get_training_emission(file_number)
#     meas_line = get_measurement(original, transfer_mat="line")
#     meas_vol = get_measurement(original)

#     params_line = optimize_params(meas_line, [400000, 0.15], "line", "simple_rbf")
#     params_vol = optimize_params(meas_vol, [400000, 0.15], "volume", "simple_rbf")

#     fit_line, cov_line = performGP(meas_line, params_line, transfer_mat="line", cov_func="simple_rbf", format="image")  
#     fit_vol, cov_vol = performGP(meas_vol, params_vol, transfer_mat="volume", cov_func="simple_rbf", format="image")

#     fig, (ax1, ax2) = plt.subplots(1,2, sharey=True, figsize=(10,3.2), gridspec_kw = {'wspace':0, 'hspace':0})

#     ax1.errorbar(range(1,129), meas_line/1000, yerr = get_std(meas_line)/1000, fmt=".k", color="black", markersize=5, capsize=2, label="input line matrix", zorder=1)
#     ax2.errorbar(range(1,129), meas_vol/1000, yerr = get_std(meas_vol)/1000, fmt=".k", color="black", markersize=5, capsize=2, label="input volume matrix", zorder=1)
#     ax1.scatter(range(1,129), get_measurement(fit_line)/1000, marker="x", label="reconstruction line matrix", color="orange", zorder=10, s=10)
#     ax2.scatter(range(1,129), get_measurement(fit_vol)/1000, marker="x", label="reconstruction volume matrix", color="orange", zorder=10, s=10)
#     ax1.legend()
#     ax2.legend()
#     ax1.set_ylabel("Signal in kW")
#     fig.text(0.48,0,"Channel")
#     shot, time = map_training_to_real_shot(file_number)
#     fig.suptitle("Comparison of measurements for shot {} at t={}s".format(shot, time))
#     plot_saver("NN_{}_measurement_comparison.png".format(file_number), dpi=350)

# plt.show()
# for file_number in [4]:
#     original = get_training_emission(file_number)
#     meas_line = get_measurement(original, transfer_mat="line")
#     meas_vol = get_measurement(original)

#     params_line = optimize_params(meas_line, [400000, 0.15], "line", "simple_rbf")
#     params_vol = optimize_params(meas_vol, [400000, 0.15], "volume", "simple_rbf")

#     fit_line, cov_line = performGP(meas_line, params_line, transfer_mat="line", cov_func="simple_rbf", format="image")  
#     fit_vol, cov_vol = performGP(meas_vol, params_vol, transfer_mat="volume", cov_func="simple_rbf", format="image")

#     line_volume_fit_comparison(file_number, [original, fit_vol, fit_line], [cov_vol, cov_line], params_vol, params_line)
#     plot_saver("NN_{}_fit_comparison.png".format(file_number), dpi=300)

# original = get_training_emission(67)
# meas = get_measurement(original)
# params_short = np.array([472000, 0.03])
# params_opt =  np.array([472000, 0.13])
# params_long =  np.array([472000, 0.50])
# fit_short, _ = performGP(meas, params_short, transfer_mat="volume", cov_func="simple_rbf", format="image")
# fit_opt, _ = performGP(meas, params_opt, transfer_mat="volume", cov_func="simple_rbf", format="image")
# fit_long, _ = performGP(meas, params_long, transfer_mat="volume", cov_func="simple_rbf", format="image")

# title1 = r"$\bf{A)}$  l=3.0 cm"
# title2 = r"$\bf{B)}$   l=13 cm" 
# title3 = r"$\bf{C)}$   l=50 cm" 

# compare_several([original, fit_short, fit_opt, fit_long], ["original", title1, title2, title3], figsize=(10,4.8))
# plot_saver("NN_67_legth_scale_compare.png", dpi=300)


# original = get_training_emission(26)
# meas = get_measurement(original)
# params_stat = optimize_params(meas, [400000, 0.15], "volume", "simple_rbf")
# #params_non = optimize_params(meas, [250000, 0.17, 0.01, -0.7, -5], "volume", "rbf_sigmoid_z")
# params_non = optimize_params(meas, [500000, 0.12, 0.05, -0.7, -5], "volume", "rbf_sigmoid_z")
# #print(params_non)
# #params_non = np.array([7.26205096e+05,  9.82840976e-02,  2.27214189e-01, -5.68115380e-01, -9.78945967e+00])
# #params_non = np.array([3.17579509e+06,1.66010567e-01,1.37099366e-01,-5.74709006e-01,-1.20427973e+01])

# fit_stat, _ = performGP(meas, params_stat, transfer_mat="volume", cov_func="simple_rbf", format="image")
# fit_non, _ = performGP(meas, params_non, transfer_mat="volume", cov_func="rbf_sigmoid_z", format="image")

# scales = np.zeros((42,23))
# z_coords = np.loadtxt("data/z_coordinates_reduced.txt")
# scales_z = np.array([length_scale(z, params_non) for z in z_coords])
# print(z_coords.shape)

# for i in range(23):
#     scales[:,i]= scales_z

# compare_stationary_nonstationary(26, [original, fit_stat, fit_non], scales)
# plot_saver("NN26_nonstat_compare.png", dpi=300)






# plots for appendix

import matplotlib.gridspec as gridspec
from mpl_toolkits.axes_grid1 import make_axes_locatable

mask = np.loadtxt("data/mask.txt")
r_coord = np.loadtxt("data/r_coordinates_reduced.txt")
z_coord = np.loadtxt("data/z_coordinates_reduced.txt")

outer_grid = gridspec.GridSpec(1,3)

start = 73
#params_init = [[300000, 0.1, 0.2, -0.7, -5], [500000, 0.12, 0.05, -0.7, -5], [900000, 0.07, 0.4, -0.5, -6]]
#params_init = [[250000, 0.17, 0.01, -0.7, -5], [250000, 0.17, 0.01, -0.7, -5], [250000, 0.17, 0.01, -0.7, -5]]
#params_init = [[300000, 0.8, 0.06, -0.6, -5], [300000, 0.8, 0.06, -0.6, -5], [300000, 0.8, 0.06, -0.6, -5]]
params_init = [[500000, 0.07, 0.07, -0.7, -5], [500000, 0.07, 0.07, -0.7, -5], [500000, 0.07, 0.07, -0.7, -5]]
params = []

for i, number in enumerate(range(start, start+3)):
    scale = 1
    original = get_training_emission(number)
    if original.max()<2000000: scale=2000000/original.max()
    original = original*scale
    measurement = get_measurement(original)
    params_opt = optimize_params(measurement, np.array([400000, 0.15]), "volume","simple_rbf")
    params.append(params_opt)

fig = plt.figure(figsize=(10,3.2))
params = np.array(params)

for i, number in enumerate(range(start,start+3)):
    
    original = get_training_emission(number)
    scale = 1.0
    if original.max()<2000000: scale = 2000000/original.max()
    original=original*scale
    measurement = get_measurement(original)
    fit, _ = performGP(measurement, params[i], cov_func="simple_rbf", format="image")
    original = original/scale
    fit = fit/scale
    
    original = original/1000000
    fit = fit/1000000

    inner_grid = gridspec.GridSpecFromSubplotSpec(1,2, subplot_spec=outer_grid[i], wspace=0.05, width_ratios=[1,1.09])
    ax1 = fig.add_subplot(inner_grid[0])
    ax2 = fig.add_subplot(inner_grid[1], sharex=ax1, sharey=ax1)
    ax2.set_yticks([])
    ax2.set_xticks([])

    ax1.set_title("original", fontsize=10)
    ax2.set_title("reconstruction", fontsize=10)

    ax1.contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors="white")
    ax2.contour(r_coord, z_coord, mask[::-1], levels=[0.5], colors="white")

    ax1.imshow(original, interpolation="spline36", 
            extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]])
    img = ax2.imshow(fit, interpolation="spline36",  vmin=0, vmax=original.max(), 
                extent=[r_coord[0], r_coord[-1], z_coord[0], z_coord[-1]])
    divider = make_axes_locatable(ax2)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    fig.colorbar(img, cax=cax)

shot1, time1 = map_training_to_real_shot(start)
#shot2, time2 = map_training_to_real_shot(start+1)
#shot3, time3 = map_training_to_real_shot(start+2)

fig.text(0.16, 0.96, "Shot {} at t={}s".format(int(shot1), time1), ha="center", fontsize=11)
#fig.text(0.48, 0.96, "Shot {} at t={}s".format(int(shot2), time2), ha="center", fontsize=11)
#fig.text(0.81, 0.96, "Shot {} at t={}s".format(int(shot3), time3), ha="center", fontsize=11)

plt.tight_layout()
plot_saver("appendix_{}_{}.png".format(start, start+2))
plt.show()