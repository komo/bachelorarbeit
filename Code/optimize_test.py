
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
from src.gp_inference import performGP, neg_log_likelihood, length_scale
from src.forward_model import get_training_emission, get_measurement
from src.optimize import optimize_params
from src.plot_helper import plot_emission, plot_saver
from src.error_model import get_std
from src.metrics import *

mask = np.loadtxt("data/mask.txt")

def analyze_shot_optimization(  filenumber, 
                                params_initial, 
                                cov_func, 
                                trans_mat   ):
    
    emission = get_training_emission(filenumber)

    measurement = get_measurement(emission)

    fit1, _ = performGP(measurement, 
                        params_initial, 
                        transfer_mat=trans_mat, 
                        cov_func=cov_func,
                        format="image"  )
    
    scale = 1.0
    if emission.max() < 200000 and False:
        print("rescale", 2000000/emission.max())
        scale = 2000000/emission.max()

    emission = emission*scale
    measurement = get_measurement(emission)
    emission = emission/scale

    opt = optimize_params(measurement, params_initial, trans_mat, cov_func)
    
    #opt[0] = opt[0]/scale
    print(opt)

    fit2, covPost2 = performGP( measurement, 
                                opt, 
                                transfer_mat=trans_mat, 
                                cov_func=cov_func,
                                format="image"  )

    
    fit2 = fit2/scale
    print(nrmse(emission, fit1), nrmse(emission, fit2))
    nnl_opt = neg_log_likelihood(measurement, opt, trans_mat, cov_func)
    nnl_initial = neg_log_likelihood(measurement, params_initial, 
                                        trans_mat, cov_func)


    compare_emissions(emission, fit1, fit2, covPost2, params_initial, 
                    opt, filenumber, nnl_initial, nnl_opt)
    #compare_with_forwardmodel(measurement, fit2)
    plot_log_likelyhood(measurement, opt, trans_mat, cov_func)


def compare_with_forwardmodel(measurement, resulting_emission):
    
    plt.figure(figsize=(9,3.5))
    error = get_std(measurement)
    plt.errorbar(np.arange(1,129), measurement/1000, yerr = error/1000, fmt=".k", markersize=8, capsize=2,
                    label="input measurements", zorder=0)
    meas_from_GP = get_measurement(resulting_emission)
    #print(meas_from_GP.min())
    plt.scatter(np.arange(1,129), meas_from_GP/1000, color="orange", 
                    label="pseudo signal via forwardmodel", s=20, zorder=10, marker="x")
    plt.xlabel("Channel")
    plt.ylabel("Signal in kW")
    plt.title("Comparison of input signal and pseudo signal")
    plt.legend()
    #plot_saver("NN66_compare_forwardmodell")


def plot_log_likelyhood(measurement, params_opt, trans_mat, cov_func):
    fig, ax1 = plt.subplots(1,1, figsize = (4,3.3))
    ax1.set_title("Logarithmic marginal likelihood")

    size = 48

    params_sigma = np.arange(0.82*params_opt[0], 1.18*params_opt[0], 
                            (1.18*params_opt[0]-0.82*params_opt[0])/size)
    params_L = np.arange(0.82*params_opt[1], 1.18*params_opt[1], 
                            (1.18*params_opt[1]-0.82*params_opt[1])/size)

    points1 = np.zeros((size, size))
    points2 = np.zeros((size, size))

    for i in range(size):
        for j in range(size):
            res1 = neg_log_likelihood(measurement, 
                                        np.array([params_sigma[i], params_L[j]]), 
                                        trans_mat, cov_func)

            points1[i,j] = -res1
            points2[i,j] = 1/(res1-1450)
            print("{} of {} pixels    {}%".format(i*size+j+1, size**2, round((i*size+j+1)*100/size**2,1)))
            
    img1 = ax1.imshow(points1, extent=[params_sigma[0]/1000, params_sigma[-1]/1000, params_L[0]*100, params_L[-1]*100], aspect="auto", cmap="plasma", interpolation="spline36")
    #img2 = ax2.imshow(points2, extent=[params_sigma[0], params_sigma[-1], params_L[0], params_L[-1]], aspect="auto")
    fig.colorbar(img1, ax=ax1)
    #fig.colorbar(img2, ax=ax2)
    ax1.scatter(params_opt[0]/1000, params_opt[1]*100, color="black", label="maximum")
    ax1.set_ylabel("length scale in cm")
    ax1.set_xlabel(r"$\sigma_E$ in kW/m³")
    ax1.legend(framealpha=0.6)
    #ax2.scatter([params_opt[0]], params_opt[1], color="r")
    #ax2.set_ylabel("length scale in m")
    #ax2.set_xlabel("sigma_f")
    plt.savefig("logarithm_optimized.png", bbox_inches='tight', dpi=300, transparent=True)

def compare_emissions(  original, 
                        initial, 
                        optimized,
                        cov_optimized,
                        params_initial, 
                        params_opt, 
                        filenumber,
                        nnl_initial,
                        nnl_opt     ):

    fig, axes = plt.subplots(2,3, figsize=(12, 10))

    if (covariance_function=="rbf_sigmoid_z"):
        title2 = r"Initial: $\sigma_E=$" + str(round(params_initial[0],4))        \
                    + "; p1=" + str(round(params_initial[1],4)) + ";\n p2=" +         \
                    str(round(params_initial[2],4)) + "; p3=" + str(round(params_initial[3],4))                 \
                    + ";\n p4=" + str(round(params_initial[4],4)) + "; nll="+ str(round(nnl_initial, 2))

        title3 = r"Optimized: $\sigma_E=$" + str(round(params_opt[0],4))        \
                    + "; p1=" + str(round(params_opt[1],4)) + ";\n p2=" +         \
                    str(round(params_opt[2],4)) + "; p3=" + str(round(params_opt[3],4))                 \
                    + ";\n p4=" + str(round(params_opt[4],4)) + "; nll="+ str(round(nnl_opt, 2))

        scales = np.zeros((42,23))
        z_coords = np.loadtxt("data/z_coordinates_reduced.txt")
        scales_z = np.array([length_scale(z, params_opt) for z in z_coords])

        for i in range(23):
            scales[:,i]= scales_z
        plot_emission(scales, fig, axes[1][1], title="length scale", interpol=None)


    else:
        title2 = r"Initial Guess: $\sigma_f=$" + str(round(params_initial[0]))  \
                    + "; l=" + str(round(params_initial[1],4)) + ";\n nll="     \
                    + str(round(nnl_initial, 2))                            

        title3 = r"Optimized: $\sigma_f=$" + str(round(params_opt[0],4))        \
                    + "; l=" + str(round(params_opt[1],4)) + ";\n nll="         \
                    + str(round(nnl_opt, 2))

    plot_emission(original, fig, axes[0][0], title="Original")
    plot_emission(initial, fig, axes[0][1], title=title2, prevent_negative=True)
    plot_emission(optimized, fig, axes[0][2], title=title3, prevent_negative=True)
    plot_emission(np.sqrt(cov_optimized), fig, axes[1][0], title="std of fit")
    
    fig.suptitle(   "NN_ " +
                    str(filenumber) + 
                    "; non-stationary ;" + 
                    r"   $K(\vec{r}_1, \vec{r}_2) = \sigma_f^2" +
                    r"\exp(-\frac{(\vec{r}_1-\vec{r}_2)^2}{ l(z)^2 + l(z')^2})$;" + "\n" +
                    " Nelder-Mead;" + r"$l(z)=p_1+\frac{p_2}{1+\exp(-p_3(z+p_4))}$", 
                    fontsize=13)

    fig.tight_layout(rect=[0, 0, 1, 0.92])


covariance_function = "simple_rbf"
transfer_mat = "volume"
initial_params = np.array([500000, 0.12])#, 0.05, -0.7, -5])
file_number = 66

analyze_shot_optimization(file_number, initial_params, covariance_function, 
                            transfer_mat)
plt.show()