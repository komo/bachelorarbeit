"""Simple demo for one test distribution"""

import numpy as np 
import matplotlib.pyplot as plt 

from src.forward_model import get_measurement, get_training_emission
from src.gp_inference import performGP
from src.plot_helper import plot_emission, map_training_to_real_shot
from src.optimize import optimize_params

# load training sample number 67
original = get_training_emission(67)

shot, time = map_training_to_real_shot(67)
print("Shot: {} at t={} s".format(shot, time))

# get bolometer measurements via forward model. transfer_mat is optional, 
# volume is the default value
measurement = get_measurement(original, transfer_mat="volume")

# initial parameters are set to sigma_E=500 kW/m³ and l=10 cm
initial_params = np.array([500000, 0.1])

# get best parameters; transfer_mat and cov_func are optional
opt_params = optimize_params(   measurement, 
                                initial_params,
                                transfer_mat = "volume",
                                cov_func = "simple_rbf" )

# get posterior mean and covariance; transfer_mat and cov_func are optional
# format="image" is required to get the data in the right format for plotting.
# Otherwise a one dimensional array is returned for the mean and covariance
posterior_emission, posterior_covariance = performGP(   measurement, 
                                                        opt_params, 
                                                        transfer_mat = "volume",
                                                        cov_func = "simple_rbf",
                                                        format = "image"  )

fig, (ax1, ax2) = plt.subplots(1,2, figsize=(8, 5))

# plot the original emission and the reconstruction and set the colorscale
# of the reconstruction to match that of the original
plot_emission(original, fig, ax1, title="original")
plot_emission(posterior_emission, fig, ax2, title="reconstruction", 
                limits=[original.min(), original.max()])

plt.tight_layout()
plt.show()