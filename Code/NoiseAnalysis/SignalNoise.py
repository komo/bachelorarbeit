""" Analyse signal dependent noise

This script analyses how the signal noise depends on the signal level.
The background variance is subtracted from the signal variance and 
a line through the origin is fitted to the data. The slope of 
the variance for each channel is stored together with it's std and
the number of datapoints for the channel in a text file.
Additionally a scatter plot with the variance over the signal level 
and the fitted line and a simple scatter plot of the signal std over 
the signal level is created.

Note: 
    - Background signal_variance above 1e11 is discarded as outlier.

Data used: 
    data/NoiseAnalysis/signal_noise_all_channels.txt
    data/NoiseAnalysis/signal_level_all_channels.txt

Results: 
    results/SignalDependentNoise/noise_dependence_complete.txt
    results/SignalDependentNoise/var_signal_scatter_with_fit.png
    results/SignalDependentNoise/std_signal_scatter.png
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.optimize import curve_fit

line = lambda x, m : m * x

def fit_line(signal_levels, signal_var):
    popt, pcov = curve_fit(line, signal_levels, signal_var)
    return popt[0], np.sqrt(pcov[0][0])

level_data = np.loadtxt("../data/NoiseAnalysis/signal_level_all_channels.txt")
noise_data = np.loadtxt("../data/NoiseAnalysis/signal_noise_all_channels.txt")
background_noise = np.loadtxt("results/BackgroundNoise/background_noise_complete.txt")

fig, axes = plt.subplots(32,4, figsize=(4*5, 32*2.5), dpi=200)
fig.text(0.5, 0.01, 'Signal level', ha='center', fontsize=36)
fig.text(0.01, 0.5, r'$\sigma_{total}^2-\sigma_{background}^2$', 
            va='center', rotation='vertical', fontsize=36)
fig.text(0.5, 0.965, r'Removed outliers with $\sigma_{total}^2 < 1e11$', 
            ha='center', fontsize=24)
fig.suptitle(r"$\sigma_{total}^2-\sigma_{background}^2$ over signal level for 35001-36794", fontsize=40)

fig2, axes2 = plt.subplots(32,4, figsize=(4*5, 32*2.5), dpi=200)
fig2.text(0.5, 0.01, 'Signal level', ha='center', fontsize=36)
fig2.text(0.01, 0.5, r'$\sigma_{total}$', va='center', rotation='vertical', fontsize=36)
fig2.text(0.5, 0.965, r'Removed outliers with $\sigma_{total}^2 < 1e11$', ha='center', fontsize=24)
fig2.suptitle(r"$\sigma_{total}$ over signal level for 35001-36794", fontsize=42)

data_to_save = []

for i in range(128):
    x = int(i/4)
    y = i%4
    
    signal_noise = noise_data[i]
    signal_levels = level_data[i]
    indicies = np.logical_and(signal_noise>0, signal_noise**2<1e11)
    signal_noise = signal_noise[indicies]
    signal_levels = signal_levels[indicies]

    signal_dependent_var = signal_noise**2-background_noise[i,0]**2
    
    title = r"{channel}; Pts: {points};".format(channel=i+1, points=signal_noise.size)
    axes[x][y].set_title(title)
    axes[x][y].scatter(signal_levels, signal_dependent_var)
    plt.setp(axes[x][y].get_xticklabels(), rotation=20, horizontalalignment='right')
    axes2[x][y].set_title(title)
    axes2[x][y].scatter(signal_levels, signal_noise)
    plt.setp(axes2[x][y].get_xticklabels(), rotation=20, horizontalalignment='right')

    m = 0 
    error = 0

    if signal_levels.size != 0:
        x_lim = np.array([0, signal_levels.max()*1.02])
        axes[x][y].set_xlim(x_lim[0], x_lim[1])
        axes2[x][y].set_xlim(x_lim[0], x_lim[1])

        m, error = fit_line(signal_levels, signal_dependent_var)
        title_fit = title + r" m={m}$\pm${error}".format(m=round(m), error=round(error))
        axes[x][y].set_title(title_fit)
        axes[x][y].plot(x_lim, line(x_lim, m), color="r", linewidth=3)

    data_to_save.append((m, error, signal_noise.size))

data_to_save = np.array(data_to_save)
np.savetxt("results/SignalDependentNoise/noise_dependence_complete.txt", data_to_save)

fig.tight_layout(rect=[0.04, 0.02, 1, 0.96])
fig.savefig("results/SignalDependentNoise/var_signal_scatter_with_fit.png")
fig2.tight_layout(rect=[0.04, 0.02, 1, 0.96])
fig2.savefig("results/SignalDependentNoise/std_signal_scatter.png")

