import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.optimize import curve_fit

level_data = np.loadtxt("../data/NoiseAnalysis/signal_level_all_channels.txt")
noise_data = np.loadtxt("../data/NoiseAnalysis/signal_noise_all_channels.txt")
background_noise = np.loadtxt("results/BackgroundNoise/background_noise_complete.txt")

fig, axes = plt.subplots(2,2, figsize=(10,5))
fig.text(0.53, 0.01, 'Signal level in kW', ha='center', fontsize=12)
fig.text(0.01, 0.5, r'$\varepsilon_{total}^2-\varepsilon_{background}^2$ in kW²', 
            va='center', rotation='vertical', fontsize=12)
fig.text(0.5, 0.89, r'Removed outliers with $\varepsilon_{total}^2 > 100000$ kW²', 
            ha='center', fontsize=10)
fig.suptitle(r"$\varepsilon_{total}^2-\varepsilon_{background}^2$ over signal level for shot 35001-36794", fontsize=14)


line = lambda x, m : m * x

def fit_line(signal_levels, signal_var):
    try:
        popt, pcov = curve_fit(line, signal_levels, signal_var)
        if popt[0] < 0: popt[0] = 0
        return popt[0], np.sqrt(pcov[0][0])
    except:
        return  0, 0

for i, channel in enumerate([8,25,96,127]):
    x = int(i/2)
    y = i%2
    
    signal_noise = noise_data[channel-1]/1000
    signal_levels = level_data[channel-1]/1000
    indicies = np.logical_and(signal_noise>0, signal_noise**2< (1e11/1000000))
    signal_noise = signal_noise[indicies]
    signal_levels = signal_levels[indicies]

    signal_dependent_var = signal_noise**2-(background_noise[channel,0]/1000)**2
    
    title = r"Channel: {channel}; Pts: {points};".format(channel=channel, points=signal_noise.size)
    axes[x][y].set_title(title)
    axes[x][y].scatter(signal_levels, signal_dependent_var, s=5)


    m = 0 
    error = 0

    if signal_levels.size != 0:
        x_lim = np.array([0, signal_levels.max()*1.02])
        axes[x][y].set_xlim(x_lim[0], x_lim[1])

        m, error = fit_line(signal_levels, signal_dependent_var)
        title_fit = title + r" m=({m}$\pm${error}) kW²".format(m=round(m,2), error=round(error,2))
        axes[x][y].set_title(title_fit)
        axes[x][y].plot(x_lim, line(x_lim, m), color="r", linewidth=3)


fig.tight_layout(rect=[0.03, 0.02, 1, 0.9])
plt.savefig("signal_noise_sample.png", bbox_inches='tight', dpi=350, transparent=True)

mockup_data = np.zeros((128))
mockup_data[np.isnan(background_noise[:,0])] = 10


# plt.figure(figsize=(9,4))
# plt.title(r"Background noise $\varepsilon_{background}$ for all channels")
# plt.xlabel("Channel")
# plt.ylabel(r"$\varepsilon_{background}$ in kW")
# plt.bar(range(1,129), mockup_data, color="orange")
# plt.bar(range(1,129), background_noise[:,0]/1000)
# plt.xlim([0,129])

# plt.savefig("background_noise_overview.png", bbox_inches='tight', dpi=300, transparent=True)

plt.show()