#include <iostream>

using namespace std;

extern "C"
void getLongestSequence(int *data, int *start_end, int dataLength) {

    int maxIdx = 0;
    int maxLen = 0;
    int currLen = 0;
    int currIdx = 0;

    for (int i=0; i<dataLength; i++) {

        if (data[i] > 0) {
            currLen++;

            if (currLen == 1) {
                currIdx = i;
            }
        }
        else {
                if (currLen > maxLen) {
                    maxLen = currLen;
                    maxIdx = currIdx;
                    //cout << maxLen <<"\n";
                }
                currLen=0;
            }
    }

    if (currLen > maxLen) {
                    maxLen = currLen;
                    maxIdx = currIdx;
    }
    
    start_end[0] = maxIdx;
    start_end[1] = maxIdx + maxLen;
}