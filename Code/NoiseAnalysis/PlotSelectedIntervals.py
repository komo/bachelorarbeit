"""
This script saves the plots of visualize_shot_analysis in functions.py
for a single channel and a specified range of shots. The shotdata is 
retrieved via the ddw wlibary
"""

import sys
sys.path.append('/afs/ipp/aug/ads-diags/common/python/lib/')
from ddww import dd
import matplotlib.pyplot as plt
from scipy.stats import norm
import numpy as np
from scipy.stats import norm
from functions import analyse_channel, visualize_shot_analysis

#data = np.loadtxt("shots35450-35550_118.txt")

background_noise = []
signal_noise = []
channel = 118

for i, shot in enumerate(range(35450,35550)):
    try:
        file = dd.shotfile('BLB', shot)
        powFHC = file("powFHC")
        powFVC = file("powFVC")
        powFDC = file("powFDC")
        powFLX = file("powFLX")
        # only first 4 channels active in FLH
        powFLH = file("powFLH")
        powFHS = file("powFHS")

        data = np.concatenate((powFHC.data, powFVC.data, powFDC.data,
                powFLX.data, powFLH.data[:,:4], powFHS.data), axis=1)[2500:-2500, 44]

        print(shot)
        shot_data = data
        std_low, std_high, _,_, signal_level = analyse_channel(shot_data)

        background_noise.append(std_low)

        signal_noise.append((signal_level, std_high))
        if std_low > 0:
            background_noise.append(std_low)
        if std_high > 0 and signal_level > 0:
            signal_noise.append((signal_level, std_high))
        visualize_shot_analysis(shot_data, shot, channel)

    except:
        print("Error while opening Shotfile")


print("")
background_noise = np.array(background_noise)
signal_noise = np.array(signal_noise)
print(signal_noise.shape)

#np.savetxt("background_noise_118.txt", background_noise)
#np.savetxt("signal_noise_118.txt", signal_noise)

plt.close("all")
fig, (ax1, ax2) = plt.subplots(1,2)
ax1.hist(background_noise, bins=20)
ax2.scatter(signal_noise[:,0], signal_noise[:,1])
plt.figure
plt.show()
