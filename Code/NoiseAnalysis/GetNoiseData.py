"""
This script collects the noise data for all 128 channels for a specified
range of shots. The calculated background noise for all channels and shots and
the noise at a high signal level and the associated signal level for all 
channels and shots are stored in seperate text file and to be used by
BackgroundNoise.py and SignalNoise.py
The shotdata is retrieved via the ddww libary.

Results
    data/NoiseAnalysis/signal_noise_all_channels.txt
    data/NoiseAnalysis/signal_level_all_channels.txt
    data/NoiseAnalysis/background_noise_all_channels.txt
"""

import sys
sys.path.append('/afs/ipp/aug/ads-diags/common/python/lib/')
from ddww import dd
import matplotlib.pyplot as plt
from scipy.stats import norm
import numpy as np
from functions import analyse_channel
import time

start_shot = 35001
end_shot = 36795

background_noise = np.zeros((128, end_shot-start_shot))
signal_noise = np.zeros((128, end_shot-start_shot))
signal_levels = np.zeros((128, end_shot-start_shot))

for i, shot in enumerate(range(start_shot,end_shot)):

    try:

        start_time = time.time()

        file = dd.shotfile('BLB', shot)
        powFHC = file("powFHC")
        powFVC = file("powFVC")
        powFDC = file("powFDC")
        powFLX = file("powFLX")
        # only first 4 channels active in FLH
        powFLH = file("powFLH")
        powFHS = file("powFHS")

        data = np.concatenate((powFHC.data, powFVC.data, powFDC.data,
                powFLX.data, powFLH.data[:,:4], powFHS.data), axis=1)[2500:-2500]
        print(time.time()-start_time)

        for channel in range(0,128):
            std_low, std_high, _,_, signal_level = analyse_channel(data[:,channel])
            background_noise[channel,i] = std_low
            signal_noise[channel,i] = std_high
            signal_levels[channel,i] = signal_level

            #save_shot_analysis(shot_data, shot, channel)

    except Exception as e:
        print("Error while opening Shotfile")
        print(e)


print("")
background_noise = np.array(background_noise)
signal_noise = np.array(signal_noise)
signal_levels = np.array(signal_levels)
print(signal_noise.shape)
print(background_noise.shape)
print(signal_levels.shape)

np.savetxt("../data/NoiseAnalysis/signal_level_all_channels.txt", signal_levels)
np.savetxt("../data/NoiseAnalysis/background_noise_all_channels.txt", background_noise)
np.savetxt("../data/NoiseAnalysis/signal_noise_all_channels.txt", signal_noise)
