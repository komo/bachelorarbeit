"""Important functions for noise Analysis"""

import matplotlib.pyplot as plt
from scipy.stats import norm
from scipy.signal import butter, sosfilt, sosfreqz, sosfilt_zi, sosfiltfilt
import numpy as np
import ctypes
import time

lib = ctypes.cdll.LoadLibrary("cpp/longest_sequence.so")
lib.getLongestSequence.argtypes = [np.ctypeslib.ndpointer(np.int32, flags='aligned, c_contiguous'),
                                    np.ctypeslib.ndpointer(np.int32, flags='aligned, c_contiguous'),
                                    ctypes.c_int]

# load time basis for each shot. First and last second are omitted
# signal recoring starts one second before shot. Therefore time_basis
# starts at t=0
time_basis = np.loadtxt("bolometer_time.txt")[2500:-2500]

def butter_highpass(highcut, fs, order):
    nyq=0.5+fs
    high = highcut/nyq
    sos = butter(order, high, analog=False, btype="high", output='sos' )
    return sos

def butter_highpass_filter(data, highcut, fs, order=3):
    sos = butter_highpass(highcut, fs, order=order)
    return sosfiltfilt(sos, data)

def get_longest_sequence(indices):
    """
    Get longest first and last index of longest sequence consisting of values
    larger than 0. Utilizes cpp function defined in ccp/longest_sequence.cpp

    Parameters
    ----------
    indeces : np.ndarray
        could be any array but in this context boolean arrays are used
    
    Returns
    -------
    int
        first index of sequence
    int
        last index of sequence
    """
    start_stop = np.zeros((2)).astype(np.int32)
    lib.getLongestSequence(indices, start_stop, indices.size)
    return start_stop[0], start_stop[1]


def get_longest_signal_sequence(data, threshold=100000):
    """
    Returns the optimal sequence of a shot to calculate the noise at a high 
    signal level. The minimal signal level to be considered is defined as 
    threshold. First the most common signal level of the lowpass signal 
    above the threshold is determined by a histogram. Then all indecies of 
    signals within a interval defined by cutoff are calculated and passed 
    into get_longest_sequence. Sequences shorter than one seconds are omitted, 
    to ensure that enough data points are available to make statistically sound
    calculations for the signals standard deviation. It also prevents data from 
    shots with highly fluctuating signals caused by physical phenomema which 
    could affect the signal noise.

    Parameters
    ----------
    data : np.ndarray
        array of the lowpass signal
    threshold : int
        defines the minimal signal level that is considered, default=100000
    
    Returns
    -------
    int
        first index of optimal signal interval
    int
        last index of optimal signal interval
    int
        signal level of interval
    """

    x,y = np.histogram(data[data>threshold], bins=100)
    xy_max = y[x.argmax()]

    if not xy_max > 0: return 0,0,0
    cutoff = xy_max*0.1
    indices = np.logical_and(data > xy_max-cutoff, data < xy_max+cutoff).astype(np.int32)
    start, end = get_longest_sequence(indices)
    if (end-start)/2500 < 1: # ommit sequences shorter than one second
        return 0,0,0
    else:
        return start, end, xy_max

def get_longest_zero_sequence(data, cutoff=4000):
    """
    Returns the optimal sequence of data to calculate the background noise.
    The indices of the lowpass signal laying within a interval defined by
    cutoff are calcualted and passed to get_longest_sequence

    Parameters
    ----------
    data : np.ndarray
        array of lowpass signal
    cutoff : int
        defining the size of the interval around the 0 line

    Returns
    -------
    int
        first index of optimal signal interval
    int
        last index of optimal signal interval
    """

    indices = np.logical_and(data > 0-cutoff, data < 0+cutoff).astype(np.int32)
    return get_longest_sequence(indices)

def get_seconds(time_step, steps_per_second=2500):
    """
    Returns time in seconds for given time_step. Shot data is recorded
    with 2500 datapoints per second 
    """
    return round((time_step/steps_per_second), 1)

def filter_data(data, cutoff=30, samplingrate=10000):
    """
    Calculate filtered shot signal

    Parameters
    ----------
    data : np.ndarray
        original signal
    cutoff : int, default=30 
        cutoff frequency for filter
    samplingrate : int, default=10000
        samplingrate for filter
    
    Returns
    -------
    highpass_data : np.ndarray
        highpass filtered signal
    lowpass_data : np.ndarray
        lowpass filtered signal
    """

    highpass_data = butter_highpass_filter(data, cutoff, samplingrate, order=3)
    lowpass_data = data - highpass_data
    return highpass_data, lowpass_data

def analyse_channel(data):
    """
    Calculate the background noise and if possible the noise at a high signal level
    for given shot data.

    Parameters
    ----------
    data : np.ndarray
        unfiltered data of single channel for one shot

    Returns
    -------
    std_low : int
        standard deviation of background for interval determined by 
        get_longest_zero_sequence
    std_high : int
        standard deviation at high signal level determined by 
        get_longest_signal_sequence
    (int, int)
        first and last index of background interval
    (int, int)
        first and last index of high signal interval
    signal_level : int
        signal level of high signal interval
    """

    highpass_data, lowpass_data = filter_data(data)
    high_start, high_end, signal_level = get_longest_signal_sequence(lowpass_data)

    low_start, low_end = get_longest_zero_sequence(lowpass_data)

    high_data = highpass_data[high_start:high_end]
    low_data = highpass_data[low_start:low_end]

    _, std_high = norm.fit(high_data)
    _, std_low = norm.fit(low_data)
    return std_low, std_high, (low_start, low_end), (high_start, high_end), signal_level

def visualize_shot_analysis(shot_data, shot_number, channel_number):
    """
    Creates and saves plot visualizing the shot analysis. The Plot
    Contains the original, lowpass and highpass signal over time and
    two histograms showing the distribution of Signals from the determined
    background interval and high signal interval. Both intervals are visualized 
    in the plots by different colors.

    Plots are stored in 
        NoiseAnalysis/results/plots/channel_number/shot_number.png

    Parameters
    ----------
    shot_data : np.ndarray
        data of single channel for one shot
    shot_number : int
        required for filename of plot
    channel_number
        required for filename of plot
    """

    std_low, std_high, low_indices, high_indices, signal_level = analyse_channel(shot_data)
    highpass_data, lowpass_data = filter_data(shot_data)

    lowtime = time_basis[low_indices[0]:low_indices[1]]
    hightime = time_basis[high_indices[0]:high_indices[1]]

    fig = plt.figure(figsize=(10,12))
    grid = plt.GridSpec(4,2, hspace=0.6)
    ax0 = fig.add_subplot(grid[0,:])
    ax1 = fig.add_subplot(grid[1,:])
    ax2 = fig.add_subplot(grid[2,:])
    ax3 = fig.add_subplot(grid[3,0])
    ax4 = fig.add_subplot(grid[3,1])

    ax0.set_title("Shot: " + str(shot_number), fontsize=20)
    ax0.set_ylabel("Signal")
    ax0.set_xlabel("Time in s")
    ax0.plot(time_basis, shot_data)
    ax0.plot(lowtime, shot_data[low_indices[0]:low_indices[1]], color="r")

    ax1.set_title("Lowpass;   Signal level: " + str(round(signal_level)))
    ax1.set_ylabel("Signal")
    ax1.set_xlabel("Time in s")
    ax1.plot(time_basis, lowpass_data)
    ax1.plot(lowtime, lowpass_data[low_indices[0]:low_indices[1]], color="r", linewidth=3)

    ax2.set_title("Highpass;   Signal level: " + str(round(signal_level)))
    ax2.set_ylabel("Signal")
    ax2.set_xlabel("Time in s")
    ax2.plot(time_basis, highpass_data)
    ax2.plot(lowtime, highpass_data[low_indices[0]:low_indices[1]], color="r")


    ax3.set_ylabel("Frequency")
    ax3.set_xlabel("Signal")
    ax3.hist(highpass_data[low_indices[0]:low_indices[1]], bins=50, color="r")
    ax3.set_title("Highpass from " + str(get_seconds(low_indices[0])) + "-" +
                    str(get_seconds(low_indices[1])) + " sec;  std: " + str(round(std_low)))


    if signal_level > 0 and (high_indices[1]-high_indices[0])/2500 > 1:
        ax0.plot(hightime, shot_data[high_indices[0]:high_indices[1]], color="orange")
        ax1.plot(hightime, lowpass_data[high_indices[0]:high_indices[1]], color="orange", linewidth=3)
        ax2.plot(hightime, highpass_data[high_indices[0]:high_indices[1]], color="orange")
        ax4.set_ylabel("Frequency")
        ax4.set_xlabel("Signal")
        ax4.hist(highpass_data[high_indices[0]:high_indices[1]], bins=50, color="orange")
        ax4.set_title("Highpass from " + str(get_seconds(high_indices[0])) + "-" +
                        str(get_seconds(high_indices[1])) +  " sec;  std: " + str(round(std_high)))
    else:
        ax4.set_title("Error: no suitable intervall")


    plt.setp(ax4.get_xticklabels(), rotation=20, horizontalalignment='right')
    plt.setp(ax3.get_xticklabels(), rotation=20, horizontalalignment='right')
    plt.savefig("results/plots/"+str(channel_number)+"/"+str(shot_number))
    plt.close(fig)
