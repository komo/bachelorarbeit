""" 
Old!. Predecessor to visualize_shot_analysis in function.py 
time interval of high signal level is manually specified
"""

import sys
sys.path.append('/afs/ipp/aug/ads-diags/common/python/lib/')
from ddww import dd
import matplotlib.pyplot as plt
from scipy.stats import norm
import numpy as np

shot = 35490
channel = 1

high_start = int(2.5*2500) # 1 second = 2500 steps recording starts at t = -1 second
high_end = int(7*2500)

low_start = int(10.5*2500)
low_end = 14*2500

time_basis = np.loadtxt("bolometer_time.txt")[2500:-2500]

file = dd.shotfile('BLB', shot)
powFHC = file("powFHC")
powFVC = file("powFVC")
powFDC = file("powFDC")
powFLX = file("powFLX")
# only first 4 channels active in FLH
powFLH = file("powFLH")
powFHS = file("powFHS")

data = np.concatenate((powFHC.data, powFVC.data, powFDC.data,
        powFLX.data, powFLH.data[:,:4], powFHS.data), axis=1)[2500:-2500, channel-1]

high_data = data[high_start:high_end]
low_data = data[low_start:low_end]

mu_high, std_high = norm.fit(high_data)
mu_low, std_low = norm.fit(low_data)

fig = plt.figure(figsize=(10,7))
grid = plt.GridSpec(2,2, wspace=0.4, hspace=0.3)
ax1 = fig.add_subplot(grid[0,:])
ax2 = fig.add_subplot(grid[1,0])
ax3 = fig.add_subplot(grid[1,1])

ax1.plot(time_basis, data)
ax1.set_title("Shot: " + str(shot) + "; Channel: " + str(channel))
ax2.hist(low_data, bins=50)
ax2.set_title("Low data from " + str(low_start/2500) + "-" +
                str(low_end/2500) +  " sec;  std: " + str(std_low))
ax3.hist(high_data, bins=50)
ax3.set_title("High data from " + str(high_start/2500) + "-" +
                str(high_end/2500) +  " sec;  std: " + str(std_high))

plt.setp(ax2.get_xticklabels(), rotation=20, horizontalalignment='right')
plt.setp(ax3.get_xticklabels(), rotation=20, horizontalalignment='right')
plt.show()
