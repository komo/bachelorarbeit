""" Background noise analysis

This script analyses and calculates the mean background noise (as standard
deviation from the 0 line) for each channel.
It takes the the background noise for all channels and all shots and 
and generates a plot with Histograms of the background noise over the different
shots for all channels and a bar chart containing the mean noise for each
channel. The results for the mean background noise are stored together with 
their std and the amount of data points that were used for the calculation

Note: 
    - Background noise above 500000 is discarded as outlier.

Data used: 
    data/NoiseAnalysis/background_noise_all_channels.txt

Results: 
    NoiseAnalysis/results/BackgroundNoise/background_noise_complete.txt
    NoiseAnalysis/results/BackgroundNoise/background_hist.png
    NoiseAnalysis/results/BackgroundNoise/background_overview.png
"""

import numpy as np
import matplotlib.pyplot as plt

background_file_path = "../data/NoiseAnalysis/background_noise_all_channels.txt"
background_data = np.loadtxt(background_file_path)

fig, axes = plt.subplots(32,4, figsize=(4*4, 32*2.5), dpi=200)
fig.text(0.5, 0.01, 'Background noise', ha='center', fontsize=36)
fig.text(0.01, 0.5, 'Frequency', va='center', rotation='vertical', fontsize=36)
fig.text(0.5, 0.97, 'Removed outliers above 500000', ha='center', fontsize=24)
fig.suptitle(r"Background noise distribution for 35001-36794", fontsize=42)

data_to_save = []

for i in range(128):
    x = int(i/4)
    y = i%4
    
    data = background_data[i]
    # remove outliers above 500000
    data = data[np.logical_and(data>0, data<500000)]
    mean = data.mean()
    std = data.std()

    data_to_save.append((mean, std, data.size))

    title = r"CH: {channel}; Pts: {points}; $\mu$={mean}; $\sigma$={std}" \
        .format(channel=i+1, points=data.size, mean=round(mean), std=round(std))
    axes[x][y].set_title(title)
    axes[x][y].hist(data, bins=40)
    plt.setp(axes[x][y].get_xticklabels(), rotation=20, 
                horizontalalignment='right')

data_to_save = np.array(data_to_save)
np.savetxt("results/BackgroundNoise/background_noise_complete.txt",data_to_save)

fig.tight_layout(rect=[0.03, 0.02, 1, 0.96])
fig.savefig("results/BackgroundNoise/background_hist.png")

mockup_data = np.zeros((128))
mockup_data[np.isnan(data_to_save[:,0])] = 10000

fig2 = plt.figure(figsize=(12, 6), dpi=200)
plt.bar(range(1,129), data_to_save[:, 0])
plt.bar(range(1,129), mockup_data, color="r")
plt.title("Background noise per channel")
plt.ylabel("Background noise")
plt.xlabel("Channel")
fig2.savefig("results/BackgroundNoise/background_overview.png")
plt.show()