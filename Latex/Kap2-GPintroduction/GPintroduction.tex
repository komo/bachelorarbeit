\chapter{Gaussian Processes - An Introduction}

\section{Multivariate Normal Distributions}

In the following the terms normal distribution and Gaussian distribution will be used
synonymously.\\
A N-dimensional multivariate normal distribution of random variables 
\(\vec{x} = (x_1,x_2,...,x_N)^T\) is given by a mean vector \(\vec{\mu}\) and a positive 
semi-definite symmetric
covariance matrix \(\mathbf{\Sigma}\) The following notation is used to denote 
that \(\vec{x}\) follows a multivariate normal distribution
\begin{equation}
  \vec{x}\ \sim\ \mathcal{N(\vec{\mu}, \mathbf{\Sigma})}
\end{equation}

The vector \(\vec{\mu} = \mathrm{E}[\vec{x}]\) contains the mean values for each 
random variable, while the diagonal elements \(\sigma_i^2\) of the covariance matrix 
contain the variance and the off-diagonal elements \(\sigma_{ij}\) specify the 
correlation between the i-th  and j-th variable. \(\mathbf{\Sigma}\) can be written as
\begin{equation}
\mathbf{\Sigma} = \mathrm{cov}(\vec{x},\vec{x}) = 
\mathrm{E}[(\vec{x} - \vec{\mu})(\vec{x} - \vec{\mu})^T]
\end{equation}

For a single random variable \(x\) the probability density function (PDF) 
of a Gaussian distribution is given by
\begin{equation}
  p(x) = \frac{1}{\sqrt{2\pi \sigma^2}}
  \exp\left[-\frac{1}{2}\left(\frac{x-\mu}{\sigma}\right)^2\right]
\end{equation}

where \(\mu\) ist the expected/mean value of \(x\) and \(\sigma^2\) is its
variance. For the multivariate case with \(N\) variables, this can be rewritten as 
\cite[section 2.3]{Bishop}:
\begin{equation}\label{kap2_eq_mvn_pdf}
  p(\vec{x}) = \frac{1}{\sqrt{(2\pi)^N\mathrm{det}(\mathbf{\Sigma})}}
  \exp\left[-\frac{1}{2} (\vec{x}-\vec{\mu})^T\mathbf{\Sigma}^{-1}(\vec{x}-\vec{\mu}) \right]
\end{equation}

Fig. \ref{kap2_img_mvn_example} shows a simple example of multivariate normal distributions for 
two variables.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{img/mvn_dist_example.png}
    \caption[Example of multivariate normal distribution]
    {Two examples of a PDF for a Gaussian distribution with two variables 
    \(x\) and \(y\).
    In both cases the mean vector is \(\vec{\mu} = (3,3)\) and the standard
    deviation for \(x\) and \(y\) is 1. \textbf{A)} shows a distribution with no 
    correlation between \(x\) and \(y\), while \textbf{B)} is a distribution 
    with a correlation of 0.8 between \(x\) and \(y\).}
    \label{kap2_img_mvn_example}
\end{figure}

\subsection{Marginalization}\label{kap2_marginalization}

Sometimes only a subset of the random variables is of interest. The probability
density of the subset can be obtained by integrating over all variables that are not 
of interest. This is called marginalization.

Take a distribution of \(N\) random variables with \(\vec{x}\) and \(\vec{y}\) being
disjoint subsets of dimension \(p\) and \(q\) such that \(p+q = N\). The probability
density function for \(\vec{x}\) is then given by
\begin{equation}\label{kap2_eq_marg_integration}
  p(\vec{x}) = \int_{-\infty}^{\infty}\int_{-\infty}^{\infty}\dots\int_{-\infty}^{\infty}
  p(\vec{x}, \vec{y})dy_1dy_2\dots dy_q
\end{equation} 

For a multivariate normal distribution each subset of variables is again another 
multivariate normal distribution.
\begin{equation}\label{kap2_eq_mvn_marginalization}
  \begin{bmatrix}
    \vec{x}\\
    \vec{y}
  \end{bmatrix}\ 
  \sim\ \mathcal{N}\left(
    \begin{bmatrix}
      \vec{\mu}_x\\
      \vec{\mu}_y
    \end{bmatrix},
    \begin{bmatrix}
      \mathbf{\Sigma}_{xx} & \mathbf{\Sigma}_{xy}\\
      \mathbf{\Sigma}_{yx} & \mathbf{\Sigma}_{yy}
    \end{bmatrix}
  \right)
\end{equation}

Therefore the distribution for \(\vec{x}\) can be obtained simply by dropping \(\vec{y}\)
and looking only at \(\mathcal{N}(\vec{\mu}_x, \mathbf{\Sigma}_{xx})\).


\subsection{Conditioning}\label{kap2_conditioning}

Often a constraint for some variables is given and the distribution of the remaining 
unconstrained variables is of interest. This is called conditioning. 

Let \(\vec{x}\) and \(\vec{y}\) be again a subset of \(N\) normal distributed variables.
The distribution for \(\vec{x}\) under the condition of \(\vec{y}\) being a concrete 
value \(\vec{a}\) is 
\begin{equation}
  (\vec{x}|\vec{y}=\vec{a})\ \sim\ \mathcal{N}(\vec{\mu}_{x|a}, \mathbf{\Sigma}_{x|a})
\end{equation}

with \(\vec{\mu}_{x|a}\) and \(\mathbf{\Sigma}_{x|a}\) given by
\begin{equation}
  \vec{\mu}_{x|a} = \vec{\mu}_x + 
  \mathbf{\Sigma}_{xy}\mathbf{\Sigma}_{yy}^{-1}(\vec{a}-\vec{\mu}_y)
\end{equation}
\begin{equation}
  \mathbf{\Sigma}_{x|a} = \mathbf{\Sigma}_{xx} - 
  \mathbf{\Sigma}_{xy}\mathbf{\Sigma}_{yy}^{-1}\mathbf{\Sigma}_{yx}
\end{equation}

A proof for this is given in appendix \ref{appendix_conditional_mean_cov}.

Fig. \ref{kap2_img_marg_cond_example} illustrates the effect of marginalization
and conditioning on a two dimensional Gaussian distribution.

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{img/mvn_marg_cond_example.png}
  \caption[Illustration of marginalization and conditioning]
  {\textbf{A)} shows the same Gaussian probability density for \(x\) and \(y\) 
  as Fig. \ref{kap2_img_mvn_example}. \textbf{B)} shows the marginal 
  distribution of \(y\) where \(x\) is removed by integration and
  the renormalized distribution of \(y\) for \(x=4\). 
  Both cases would show the same distribution if \(x\) and \(y\) would be
  independent variables, i.e. the correlation between them is \(0\).}
  \label{kap2_img_marg_cond_example}
\end{figure}


\subsection{Gaussian Processes}

A Gaussian process (GP) is a generalization of a multivariate normal 
distribution from a discrete set of random variables to a distribution in function space. 
Instead of a mean vector \(\vec{\mu}\in \mathbb{R}^N\) and a covariance matrix
\(\Sigma \in \mathbb{R}^{N\times N}\), a GP is defined by a continuous mean
function and covariance function. Sometimes the covariance function is also called
kernel function. 
\begin{equation}
  f(x)\ \sim\ \mathcal{GP}[m(x), k(x,x')]
\end{equation}

A GP can be understood as a normal distribution of functions. The mean function 
\(m(x)\) and the covariance function \(k(x,x')\) specify this distribution.
\begin{equation}
  m:D \rightarrow \mathbb{R},\quad k:D\times D \rightarrow \mathbb{R}
\end{equation}

\(D\) is thereby the Domain on which the functions \(f\) are defined. 

The formal definition of a Gaussian process is that for an arbitrary set of points\linebreak
\(x_1, x_2, \dots, x_N \in D\) the distribution of \([f(x_1), f(x_2), \dots, f(x_N)]\)
is given by a N-dimensional multivariate normal distribution.
\begin{equation}\label{kap2_eq_gp_normal_dist}
  \begin{bmatrix}
    f(x_1)\\
    \vdots\\
    f(x_N)
  \end{bmatrix}
  \sim \mathcal{N}\left(
    \begin{bmatrix}
      m(x_1)\\
      \vdots\\
      m(x_N)
    \end{bmatrix},
    \begin{bmatrix}
      k(x_1,x_1) & \cdots & k(x_1, x_N)\\
      \vdots & \ddots & \vdots\\
      k(x_N,x_1) & \cdots & k(x_N, x_N)
    \end{bmatrix}
  \right)
\end{equation}

This is actually the only way to work with GPs as there is no way of sampling or 
working with infinite dimensional distributions. One always has to use a limited amount of 
support points, which have to be close enough together, so that the function
\(f(x)\) can be interpolated in between.

The covariance function \(k(x,x')\) can be interpreted as a function describing 
how strong the correlation between two points \(x,x'\in D\) is. Therefore, the 
behavior of the functions \(f(x)\) emerging from the GP are determined by the 
covariance function.

Common choices for covariance functions are \cite[chapter 4]{Rasmussen06}:
\begin{itemize}
  \item{ Gaussian Noise
  \begin{equation}
    k_{Noise}(x,x') = \sigma^2\delta_{x,x'}
  \end{equation}}
  \item{Squared Exponential:
  \begin{equation}
    k_{SE}(x,x') = \sigma^2\exp\left(-\frac{(x-x')^2}{2l^2}\right)
  \end{equation}}
  \item{Ornstein-Uhlenbeck:
  \begin{equation}
    k_{OU} = \sigma^2\exp\left(-\frac{|x-x'|}{l}\right)
  \end{equation}}
\end{itemize}

\(\sigma\) is the variance of the functions described by the GP. Many 
covariance functions also include a length scale \(l\) as parameter. The length scale 
describes how far points can be apart to still have a significant correlation. 
Functions like those above are called stationary covariance functions as they are 
invariant under translation, i.e. they only depend on the distance between two points 
but not on their individual positions. For non-stationary GPs this is not the case. 
The length scale could for example be itself a function of the coordinates which 
is not invariant under translation. This is useful when the functions that should be 
described by the GP have a varying smoothness. An example of a GP with non-stationary covariance 
is given in Fig. \ref{kap2_img_gp_non_stationary_cov}. Fig. \ref{kap2_img_gp_diff_cov}
shows stationary GPs for different covariance functions.

\newpage

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{img/gp_diff_cov_func.png}
  \caption[Demonstration of different covariance functions]
  {Sampled functions for Gaussian processes with different covariance functions. 
  For every GP the mean function was chosen to be \(m(x)=0.5x\) and 
  \(\sigma,l = 1\). The blue ribbon corresponds to one standard deviation \(\sigma\). 
  \textbf{A)} The GP with squared exponential covariance results in 
  smooth functions. \textbf{B)} Processes with Ornstein-Uhlenbeck covariance 
  are used to model the velocity of brownian motion \cite[p. 86]{Rasmussen06}. 
  \textbf{C)} In case of the Gaussian noise, there is no correlation between 
  points. This could easily be interpreted as some noisy signal/measurement.
  \textbf{D)} Here the effect of the length scale on GPs with squared exponential
  covariance is visualized. For large values the functions emerging from the GP tend to be 
  very smooth while smaller values result in more wiggly functions.}
  \label{kap2_img_gp_diff_cov}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{img/gp_non_stationary.png}
  \caption[Example for Gaussian process with non-stationary covariance function]
  {Example for a non-stationary Gaussian process. The mean function is 
  chosen to be 0. The squared exponential covariance function was modified by introducing 
  a length scale, that is itself a function of the coordinates. This function is given 
  by a Gaussian curve. The results are sampled  functions that are smoother in the center than 
  in the outer regions.}
  \label{kap2_img_gp_non_stationary_cov}
\end{figure}


\section{Gaussian Process Regression}\label{kap2_GPR}

Machine learning in general can be separated into classification and 
regression problems. The former assign a discrete label to a given input, like
handwriting recognition, where the input is the image of one 
letter and the output is the corresponding letter.
On the other hand regression problems try to fit a continuous function to given inputs.
Gaussian processes can be useful for both kind of problems. Here we will take a quick 
lock at regression with GPs.
\\
Let \(\vec{y} = (f(x_1),\dots f(x_n))^T\) be a vector with \(n\) individual measurements
taken at the points \((x_1,\dots, x_n)\) and 
\(f(x)\) be an unknown function to be fitted to the measurements. To solve this problem 
with Gaussian processes we assume that \(f(x)\) is given by a prior Gaussian process
\(f(x)\sim\mathcal{GP}[m(x), k_(x,x')]\). The mean function is often
chosen to be 0, but it can also be used to encode prior experience or expert knowledge 
about \(f(x)\) \cite[p. 27]{Rasmussen06}. It is not possible to work directly 
with distributions of functions, therefore we fit \(m\) discrete support points
\(\vec{f}_* = (f(x^*_1),\dots, f(x^*_m))^T\) at the coordinates \((x_1^*,\dots, x_m^*)\). 
The function can then be interpolated between those points. According to equation
\ref{kap2_eq_gp_normal_dist} both the measurements and  the support points adhere 
to a prior joint distribution
\begin{equation}
  \begin{bmatrix}
    \vec{y}\\
    \vec{f}_*
  \end{bmatrix}\ 
  \sim\ \mathcal{N}\left(
    \begin{bmatrix}
      \vec{\mu}_y\\
      \vec{\mu}_{f_*}
    \end{bmatrix},
  \begin{bmatrix}
    \mathbf{\Sigma}_{xx} & \mathbf{\Sigma}_{xx_*}\\
    \mathbf{\Sigma}_{x_*x} & \mathbf{\Sigma}_{x_*x_*}
  \end{bmatrix}
  \right)
\end{equation}

with
\begin{equation*}
  \mathbf{\Sigma}_{xx} =
  \begin{bmatrix}
    k(x_1,x_1) & \cdots & k(x_1, x_n)\\
    \vdots & \ddots & \vdots\\
    k(x_n,x_1) & \cdots & k(x_n, x_n)
  \end{bmatrix},\quad
  \mathbf{\Sigma}_{x_*x_*} =
  \begin{bmatrix}
    k(x^*_1,x^*_1) & \cdots & k(x^*_1, x^*_m)\\
    \vdots & \ddots & \vdots\\
    k(x^*_m,x^*_1) & \cdots & k(x^*_m, x^*_m)
  \end{bmatrix}
\end{equation*}
\begin{equation*}
  \mathbf{\Sigma}_{x_*x} = \mathbf{\Sigma}_{xx_*}^T =
  \begin{bmatrix}
    k(x^*_1,x_1) & \cdots & k(x^*_1, x_n)\\
    \vdots & \ddots & \vdots\\
    k(x^*_m,x_1) & \cdots & k(x^*_m, x_n)
  \end{bmatrix}
\end{equation*}

With given measurements \(\vec{y}\) we can condition this distribution according 
to section \ref{kap2_conditioning}. We obtain a new conditioned normal distribution, 
called the posterior distribution.
\begin{equation}
  \vec{f}_*|\vec{y}\ \sim\ \mathcal{N}(\vec{\mu}_{post}, \mathbf{\Sigma}_{x_*x_*}^{post})
\end{equation}
\begin{equation}
  \vec{\mu}_{post} = \vec{\mu}_{f_*|y} = \vec{\mu}_{f_*} + 
  \mathbf{\Sigma}_{x_*x}\mathbf{\Sigma}_{xx}^{-1}(\vec{y}-\vec{\mu}_y)
\end{equation}
\begin{equation}
  \mathbf{\Sigma}_{x_*x_*}^{post} = \mathbf{\Sigma}_{f_*|y} = \mathbf{\Sigma}_{x_*x_*} - 
  \mathbf{\Sigma}_{x_*x}\mathbf{\Sigma}_{xx}^{-1}\mathbf{\Sigma}_{xx_*}
\end{equation}

\(\vec{\mu}_{post}\) corresponds to the most likely values of the fitted function 
at positions \(x_1^*,\dots,x_m^*\)
and the diagonal elements of \(\mathbf{\Sigma}_{x_*x_*}^{post}\) to the variance
at those support points. Fig. \ref{kap2_img_fit_demo} visualizes a simple Gaussian
process regression.
\\
In most cases the measurements have some error. Currently this is not taken into 
account. In Fig. \ref{kap2_img_fit_demo}, the sampled functions go exactly to 
the measurements. To add the measurement error through our fit we can simply add 
a Gaussian noise term to our covariance for the measurements. This is possible 
as the addition of two covariance matrices is again a valid covariance matrix 
in the sense, that it is positive semi-definite and symmetric and can hence 
be used to construct a new normal distribution.
The prior joint distribution becomes
\begin{equation}
  \begin{bmatrix}
    \vec{y}\\
    \vec{f}_*
  \end{bmatrix}\ 
  \sim\ \mathcal{N}\left(
    \begin{bmatrix}
      \vec{\mu_y}\\
      \vec{\mu_{f_*}}
    \end{bmatrix},
  \begin{bmatrix}
    \mathbf{\Sigma}_{xx} + \sigma^2 \mathbf{I} & \mathbf{\Sigma}_{xx_*}\\
    \mathbf{\Sigma}_{x_*x} & \mathbf{\Sigma}_{x_*x_*}
  \end{bmatrix}
  \right)
\end{equation}

Where \(\sigma^2\) is the variance of the measurements. We obtain
\begin{equation}
  \vec{\mu}_{post} = \vec{\mu}_{f_*} + 
  \mathbf{\Sigma}_{x_*x}(\mathbf{\Sigma}_{xx} + \sigma^2 \mathbf{I})^{-1}
  (\vec{y}-\vec{\mu}_y)
\end{equation}
\begin{equation}
  \mathbf{\Sigma}_{x_*x_*}^{post} = \mathbf{\Sigma}_{x_*x_*} - 
  \mathbf{\Sigma}_{x_*x}(\mathbf{\Sigma}_{xx} + \sigma^2 \mathbf{I})^{-1}
  \mathbf{\Sigma}_{xx_*}
\end{equation}

Fig. \ref{kap2_img_gp_fit_error} shows the posterior with added error to the 
measurements

The hyperparameters (e.g. length scale) of the covariance function have a large 
impact on the regression. If the length scale is too small, the functions 
from the posterior have too much freedom a tend to overfit the measurements. On 
the other hand if the length scale is too large, the posterior might not be able to 
reproduce the behavior of the measurement well enough. 
Fig. \ref{kap2_img_gp_fit_diff_l_scale} depicts the posterior with small and large 
length scales. In section \ref{kap3_opt_param} a way for finding the optimal 
parameters for the fit is introduced.

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{img/gp_fit_demo.png}
  \caption[Gaussian process regression]
  {For this example a GP with squared exponential covariance and a 0 mean
  function was chosen. \textbf{A)} Some sampled functions for the initial 
  (prior) GP. \textbf{B)} By including measurements a constraint is posed on the 
  prior. This results in a conditioned (posterior) GP with a new mean function and 
  covariance.}
  \label{kap2_img_fit_demo}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{img/gp_fit_error.png}
  \caption[Gaussian process regression with error]
  {Posterior GP after adding an error of 0.15 to the measurements. 
  The sampled functions now correctly reproduce the measurement error.}
  \label{kap2_img_gp_fit_error}
\end{figure}

\begin{figure}[H]
  \centering
  \includegraphics[width=\textwidth]{img/gp_fit_diff_l_scale.png}
  \caption[Effect of length scale on Gaussian process regression]
  {The length scale has a large effect on the fit. \textbf{A)} Smaller 
  length scales tends to overfit the function. \textbf{B)} On the other hand, 
  larger ones result in a less accurate fit to the measurements. The optimal 
  parameters for the fit can be approximated by optimizing the marginal 
  logarithmic likelihood as described in section \ref{kap3_opt_param}.}
  \label{kap2_img_gp_fit_diff_l_scale}
\end{figure}
