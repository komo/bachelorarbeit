\chapter{Gaussian Process Tomography}\label{kap3}

In this chapter our goal is to develop a framework for obtaining an emission 
profile for given bolometer measurements. Therefore, we model both the measurements
and the emission as Gaussian processes. The bolometer measurements are given by a 
vector \(\vec{d}_m\) of size \(m\). The emission is given by an image with 
\(k\times l\) pixels, represented by a vector \(\vec{E}_n\) of size \(n=k\cdot l\).

\section{Forward Model}

If the individual measurements \(d_i\) were given by emission values for 
specific pixels the approach in \ref{kap2_GPR} could be  used to obtain the 
emission distribution.
Instead the measurements are given by integrated sight lines. The i-th measurement is 
given by
\begin{equation}\label{kap3_eq_sightline_integral}
  d_i = \int_{S_i} E(\vec{r})d\vec{r}
\end{equation}

\(S_i\) is the path of the sight line. 
The continuous emissivity function \(E(\vec{r})\) depends on the spatial coordinates 
\(\vec{r}\). By changing the continuous function \(E(\vec{r})\) to a discrete 
emissivity distribution of \(n\) pixels this can be expressed as a linear equation.
\begin{equation}
  \vec{d}_m = \mathbf{S}_{m\times n} \cdot \vec{E}_n \pm \vec{\varepsilon}
\end{equation} 
 
\(\mathbf{S}_{m\times n}\) is a transfer matrix that maps how much each 
of the \(n\) pixels of the emission distribution contributes to each of the \(m\) 
sight lines. \(\vec{\varepsilon}\) is the error/uncertainty of the measurement 
as described in section \ref{kap4_errormodel}.

ASDEX Upgrade has 6 bolometer cameras with a total of 128 different sight lines.
Two different transfer matrices for an emission distribution with 45x83 pixels were 
developed at IPP \cite{pierresPaperOld} \cite{pierresPaper}. 
The first matrix encodes the sight lines as perfect lines, 
while the second one maps the sight lines to narrow cones which is physically more accurate. 
In this case the integration in Eq. \ref{kap3_eq_sightline_integral} changes
to a volume integral over the respective cone. Fig. \ref{kap3_line_vs_volume}
illustrates the difference between both matrices.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{img/line_vs_volume.png}
    \caption[Comparision of sight line for line and volume transfer matrix]
    {Sight line for bolometer channel 28. The left plot shows the sight line
    from the line transfer matrix and the right one from the volume transfer matrix 
    where the sight lines are narrow cones instead of lines. The plots represent the
    28th row of the respective transfer matrix which yields a vector of size 3735, 
    reshaped to a 83x45 Matrix for the plot.}
    \label{kap3_line_vs_volume}
\end{figure}

Most of the following work is done with scaled down transfer matrices on emission 
distributions with 23x42 pixels.

With the transfer matrix the forward problem which is completely deterministic can be solved.
For any given emission distribution we obtain the corresponding bolometer 
measurements by multiplying the emission with the transfer matrix as shown in Fig. 
\ref{kap3_forwardmodel}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{img/forwardmodel.png}
    \caption[Forward model]
    {For a given emission distribution (left) the corresponding bolometer 
    measurements (right) are obtained via the transfer matrix. The measurement error 
    is calculated separately (see section \ref{kap4_errormodel}).}
    \label{kap3_forwardmodel}
\end{figure}

The goal is to solve the inverse problem. This solution cannot be 
unique as there are far more unknowns (966 pixels) compared to the number 
of measurements (128). To solve problems of this kind two approaches are possible. 
In the first one an underlying physical model for the unknowns is used. For the 
emission distribution it could for example be assumed that the emission 
corresponds with the magnetic flux surfaces. Once a viable model is found, 
an optimization algorithm is used to find the best parameters of the model 
for the given measurements. In the next section the second approach is explored, 
whereby no concrete underlying model is assumed. Instead both measurements and 
unknowns are modeled by a probability distribution. This makes it possible to find 
an most probable solution via bayesian inference.


\section{Bayesian Inference}
\subsection{Bayes' Theorem}

Let \(A\) and \(B\) be two events with the probability of \(B\), \(p(B) > 0\).
The conditional probability of \(A\) under the condition that \(B\) is true is 
defined as follows \cite[p. 6]{Kolmogorov}
\begin{equation}
  p(A|B) = \frac{p(A \cap B)}{p(B)}
\end{equation}

\(p(A \cap B)\) is the probability for both events \(A\) and \(B\) 
being true. By inserting \(p(A\cap B) = p(B|A)P(A)\) we get Bayes' theorem
\begin{equation}
  p(A|B) = \frac{p(B|A)p(A)}{p(B)}
\end{equation}


\subsection{Inference of Emission Distribution}\label{kap3_inference_emission}

We assume that both our measurement \(\vec{d}_m\) and our emission \(\vec{E}_n\)
follow a Gaussian process. Therefore the discrete values are given by a 
multivariate normal distribution defined by the respective covariance function. 
For \(\vec{E}_n\) a squared exponential covariance function is chosen as the 
emission is given by  a smooth function. The bolometer measurements 
are assumed to be independent and a Gaussian noise covariance function is applied.

\begin{equation}
  \vec{d}_m\ \sim\ \mathcal{N}(\vec{\mu}_d, \mathbf{\Sigma}_d),\qquad 
  \vec{E}_n\ \sim\ \mathcal{N}(\vec{\mu}_E, \mathbf{\Sigma}_E)
\end{equation}
with
\begin{equation*}
    \mathbf{\Sigma}_d = 
    \begin{bmatrix}
        \varepsilon_1^2 & \cdots & 0\\
        \vdots & \ddots & \vdots\\
        0 & \cdots & \varepsilon_m^2
    \end{bmatrix},\qquad
    \mathbf{\Sigma}_E = 
    \begin{bmatrix}
        k_{SE}(\vec{r}_1, \vec{r}_1) & \cdots & k_{SE}(\vec{r}_1, \vec{r}_n)\\
        \vdots & \ddots & \vdots\\
        k_{SE}(\vec{r}_n, \vec{r}_1) & \cdots & k_{SE}(\vec{r}_n, \vec{r}_n)
    \end{bmatrix}
\end{equation*}

\(\varepsilon_i^2\) stands for the variance of the i-th bolometer channel and 
\(\vec{r}_j\) is the spatial coordinate of the j-th emissivity pixel on the \(k\times l\)
grid.

The goal is to get the conditioned distribution for \(\vec{E}_n|\vec{d}_m\). By 
expressing \(\vec{E}_n\) and \(\vec{d}_m\) as probability densities \(p(\vec{E}_n)\) 
and \(p(\vec{d}_m)\) we can use Bayes' theorem to obtain a conditional probability 
density for \(\vec{E}_n|\vec{d}_m\).
\begin{equation}\label{kap3_eq_bay_inf}
  p(\vec{E}_n|\vec{d}_m) = \frac{p(\vec{d}_m|\vec{E}_n)p(\vec{E}_n)}{p(\vec{d}_m)}\ 
  \propto\ p(\vec{d}_m|\vec{E}_n)p(\vec{E}_n)
\end{equation}

We are only interested in the mean and covariance of the conditioned distribution. 
\(p(\vec{d}_m)\) only acts as a renormalization to the PDF and does neither influence
the mean nor the covariance. Hence we only have to look at 
\(p(\vec{d}_m|\vec{E}_n)p(\vec{E}_n)\). With Eq. \ref{kap2_eq_mvn_pdf} the PDF 
for \(\vec{E}_n\) can be expressed as
\begin{equation}\label{kap3_eq_pdf_e}
    p(\vec{E}_n) = \frac{1}{\sqrt{(2\pi)^n\mathrm{det}(\mathbf{\Sigma}_E)}}
    \exp\left[-\frac{1}{2} (\vec{E}_n-\vec{\mu}_E)^T\mathbf{\Sigma}_E^{-1}
    (\vec{E}_n-\vec{\mu}_E) \right]
\end{equation}

The PDF for \(\vec{d}_m|E_n\) is also given by a normal distribution. In this case
the distribution is centered around the measurements that would result from the given
emission. This mean is calculated via the forward model.
\begin{equation}
  \vec{\mu}_{d_m|E_n} = \mathbf{S}_{m\times n}\cdot \vec{E}_n
\end{equation}

Thus the PDF can be written as 
\begin{equation}\label{kap3_eq_pdf_d_given_e}
  p(\vec{d}_m|\vec{E}_n) = \frac{1}{\sqrt{(2\pi)^m\mathrm{det}(\mathbf{\Sigma}_d)}}
  \exp\left[-\frac{1}{2} (\vec{d}_m-\mathbf{S}\vec{E}_n)^T\mathbf{\Sigma}_d^{-1}
  (\vec{d}_m-\mathbf{S}\vec{E}_n) \right]
\end{equation}

By plugging Eq. \ref{kap3_eq_pdf_e} and \ref{kap3_eq_pdf_d_given_e} into Eq. 
\ref{kap3_eq_bay_inf} we get
\begin{equation}\label{kap3_eq_post_pdf_through_prior}
    \begin{split}
        p(\vec{E}_n|\vec{d}_m)\ \propto\ p(\vec{d}_m|\vec{E}_n)p(\vec{E}_n)\ \propto\
        \exp\left(-\frac{1}{2}\left[(\vec{E}_n-\vec{\mu}_E)^T\mathbf{\Sigma}_E^{-1}
        (\vec{E}_n-\vec{\mu}_E)\ + \right.\right. \\
        \left.\left.(\vec{d}_m-\mathbf{S}\vec{E}_n)^T\mathbf{\Sigma}_d^{-1}
        (\vec{d}_m-\mathbf{S}\vec{E}_n)\right]\right)
    \end{split}
\end{equation}

The multiplication of two Gaussians is again a (non normalized) Gaussian.
Thus, the posterior PDF \(p(\vec{E}_n|\vec{d}_m)\) is given by
\begin{equation}\label{kap3_eq_post_pdf}
    p(\vec{E}_n|\vec{d}_m) = \frac{1}{\sqrt{(2\pi)^m\mathrm{det}(\mathbf{\Sigma}_E^{post})}}
    \exp\left[-\frac{1}{2} (\vec{E}_n-\vec{\mu}_E^{post})^T[\mathbf{\Sigma}_E^{post}]^{-1}
    (\vec{E}_n-\vec{\mu}_E^{post})\right]
\end{equation}

To get some expression for \(\vec{\mu}_E^{post}\) and \(\mathbf{\Sigma}_E^{post}\) we 
expand both the exponent of Eq. \ref{kap3_eq_post_pdf_through_prior} and 
\ref{kap3_eq_post_pdf}. For the latter this yields
\begin{equation}\label{kap3_eq_post_expanded}
    \begin{split}
        \vec{E}_n^T[\mathbf{\Sigma}_E^{post}]^{-1}\vec{E}_n\ &-\ 
        \vec{E}_n^T[\mathbf{\Sigma}_E^{post}]^{-1}\vec{\mu}_E^{post}\ - \\
        (\vec{\mu}_E^{post})^T[\mathbf{\Sigma}_E^{post}]^{-1}\vec{E}_n\ &+\
        (\vec{\mu}_E^{post})^T[\mathbf{\Sigma}_E^{post}]^{-1}\vec{\mu}_E^{post}
    \end{split}
\end{equation}

Expanding Eq. \ref{kap3_eq_post_pdf_through_prior} gives
\begin{equation} 
    \begin{split}
        \vec{E}_n^T\mathbf{\Sigma}_E^{-1}\vec{E}_n\ -\  
        \vec{E}_n^T\mathbf{\Sigma}_E^{-1}\vec{\mu}_E\ &-\ 
        \vec{\mu}_E^T\mathbf{\Sigma}_E^{-1}\vec{E}_n\ +\ 
        \vec{\mu}_E^T\mathbf{\Sigma}_E^{-1}\vec{\mu}_E\ + \\
        \vec{d}_m^T\mathbf{\Sigma}_d^{-1}\vec{d}_m\ -\  
        \vec{d}_m^T\mathbf{\Sigma}_d^{-1}\mathbf{S}\vec{E}_n &-\  
        \vec{E}_n^T\mathbf{S}^T\mathbf{\Sigma}_d^{-1}\vec{d}_m\ +\  
        \vec{E}_n^T\mathbf{S}^T\mathbf{\Sigma}_d^{-1}\mathbf{S}\vec{E}_n
    \end{split}
\end{equation}

After some regrouping this becomes
\begin{equation}\label{kap3_eq_post_pdf_prior_expanded}
    \begin{split}
        \vec{E}_n^T(\mathbf{\Sigma}_E^{-1} + 
        \mathbf{S}^T\mathbf{\Sigma}_d^{-1}\mathbf{S})\vec{E}_n\ &-\ 
        \vec{E}_n^T(\mathbf{\Sigma}_E^{-1}\vec{\mu}_E + 
        \mathbf{S}^T\mathbf{\Sigma}_d^{-1}\vec{d}_m)\ - \\
        (\vec{d}_m^T\mathbf{\Sigma}_d^{-1}\mathbf{S} + 
        \vec{\mu}_E^T\mathbf{\Sigma}_E^{-1})\vec{E}_n\ &+\ 
        \vec{\mu}_E^T\mathbf{\Sigma}_E^{-1}\vec{\mu}_E\ +\  
        \vec{E}_n^T\mathbf{S}^T\mathbf{\Sigma}_d^{-1}\mathbf{S}\vec{E}_n
    \end{split}
\end{equation}

Comparing the first term of Eq. \ref{kap3_eq_post_expanded} and 
\ref{kap3_eq_post_pdf_prior_expanded} leads to
\begin{equation}
    [\mathbf{\Sigma}_E^{post}]^{-1} = (\mathbf{\Sigma}_E^{-1} + 
    \mathbf{S}^T\mathbf{\Sigma}_d^{-1}\mathbf{S})
\end{equation}

Comparison of the second terms results in 
\begin{equation}
    [\mathbf{\Sigma}_E^{post}]^{-1}\vec{\mu}_E^{post} = 
    (\mathbf{\Sigma}_E^{-1}\vec{\mu}_E + \mathbf{S}^T\mathbf{\Sigma}_d^{-1}\vec{d}_m)
\end{equation}

Consequently we get
\begin{equation}
    \vec{\mu}_E^{post} =\mathbf{\Sigma}_E^{post}(\mathbf{\Sigma}_E^{-1}\vec{\mu}_E + 
    \mathbf{S}^T\mathbf{\Sigma}_d^{-1}\vec{d}_m)
\end{equation}

In our case the mean for the emission distribution \(\vec{\mu}_E\) is set to 0. 
Thus, we finally can express the posterior mean and covariance of the emission as 

\newcommand*\widefbox[1]{\fbox{\hspace{1em}#1\hspace{1em}}}
\begin{subequations}
    \begin{empheq}[box=\widefbox]{align}
        &\vec{\mu}_E^{post} = (\mathbf{\Sigma}_E^{-1} + 
        \mathbf{S}^T\mathbf{\Sigma}_d^{-1}\mathbf{S})^{-1}
        \mathbf{S}^T\mathbf{\Sigma}_d^{-1}\vec{d}_m \\[3pt] 
        &\mathbf{\Sigma}_E^{post} = (\mathbf{\Sigma}_E^{-1} + 
        \mathbf{S}^T\mathbf{\Sigma}_d^{-1}\mathbf{S})^{-1}
    \end{empheq}
\end{subequations}


\section{Optimization of Hyperparameters}\label{kap3_opt_param}

The quality of the fitted emission distribution depends on the chosen 
covariance function and its hyperparameters. The covariance function selected 
for our tomography is given by a squared exponential
\begin{equation}\label{kap3_eq_squared_exponential}
  k_{SE}(\vec{r}_i, \vec{r}_j) = \sigma_E^2\exp
  \left(-\frac{(\vec{r}_i-\vec{r}_j)^2}{2l^2}\right)
\end{equation}

In this specific case \(\sigma_E^2\) is a hyperparameter, describing the variance
of the emission distribution. As described before, \(l\) is a characteristic length 
scale. It can be interpreted as the distance that two pixels of the emission distribution 
can be apart to still have a significant correlation. \(\vec{r}_i\) is the 
spatial coordinate of the i-th pixel of the emission distribution.

From now on we will denote the set of all hyperparameters used by a covariance function 
with \(\theta = \{\sigma, l, \dots\}\). So far we neglected the hyperparameters in our 
normal distributions, but in reality the PDFs are also functions of those. Therefore, we 
would have to write \(p(\vec{E}_n|\vec{d}_m, \theta),\ p(\vec{d}_m|\vec{E}_n, \theta),
\ p(\vec{E}_n,\theta),\dots\ \) in the equations above.
The goal is to maximize the probability of the hyperparameters for a given measurement.
According to Bayes' theorem we can write this probability as
\begin{equation}
  p(\theta|\vec{d}_m) = \frac{p(\vec{d}_m|\theta)p(\theta)}{p(\vec{d}_m)}
  \propto p(\vec{d}_m|\theta)p(\theta)
\end{equation}

Now the assumption is made that, initially without providing measurements as constraint,
all possible values for the hyperparameters are equally likely. Therefore, \(p(\theta)\) 
is a uniform probability density function, that has no impact on the position of 
the maximum of the posterior. 
Consequently, we only have to maximize \(p(\vec{d}_m|\theta)\) which is also called 
the marginal likelihood, as according to Eq. \ref{kap2_eq_marg_integration} it is 
obtained by integrating over \(\vec{E}_n\).
\begin{equation}
    p(\vec{d}_m|\theta) = \int_{\mathbb{R}^n} 
    p(\vec{d}_m|\vec{E}_n,\theta)p(\vec{E}_n|\theta)d\vec{E}_n
\end{equation}

Here the PDFs from Eq. \ref{kap3_eq_pdf_e} and \ref{kap3_eq_pdf_d_given_e} can be 
inserted. After integrating we get the following term for the logarithmic 
marginal likelihood (see appendix \ref{appendix_marg} for details)
\begin{equation}\label{kap3_eq_log_like}
    \begin{split}
    \ln(p(\vec{d}_m|\theta)) = &-\frac{m}{2}\ln(2\pi)
    - \frac{1}{2}\ln(\det[\mathbf{\Sigma}_d + \mathbf{S\Sigma}_E\mathbf{S}^T]) \\
    &- \frac{1}{2}(\vec{d}_m-\mathbf{S}\vec{\mu}_E)^T[\mathbf{\Sigma}_d + 
    \mathbf{S\Sigma}_E\mathbf{S}^T]^{-1}(\vec{d}_m-\mathbf{S}\vec{\mu}_E)
    \end{split} 
\end{equation}

By optimizing this function with regard to the hyperparameters \(\theta\)
(which are contained in \(\mathbf{\Sigma}_E\)) with one of the various optimization 
algorithms available nowadays, it is possible to estimate the best parameters for 
the given measurement.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\textwidth]{img/logarithm_optimized.png}
    \caption[Marginal logarithmic likelihood]
    {Logarithmic marginal likelihood for shot 36330 at \(t=4.6\) s
    also shown in Fig. \ref{kap4_img_prediction_good}. The maximum is 
    marked by the black dot.}
    \label{kap3_img_log_opt}
\end{figure}