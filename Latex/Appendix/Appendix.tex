\chapternumberfont{\fontsize{18pt}{18pt}\selectfont}
\chaptertitlefont{\fontsize{16pt}{20pt}\selectfont}

\chapter{Matrix Identities}\label{appendix_matrix_ident}
\section*{Basic Identities}

For two matrices \(\mathbf{A}\) and \(\mathbf{B}\) the following identities hold

\begin{equation}
  (\mathbf{AB})^T = \mathbf{B}^T\mathbf{A}^T
\end{equation}
\begin{equation}
    (\mathbf{AB})^{-1} = \mathbf{B}^{-1}\mathbf{A}^{-1}
\end{equation}
\begin{equation}
  (\mathbf{A^{-1}})^T = (\mathbf{A}^T)^{-1}
\end{equation}
\begin{equation}
  \det(\mathbf{AB}) = \det(\mathbf{A})\det(\mathbf{B})
\end{equation}


\section*{Block Matrix Inversion}

For the inversion of a block matrix \(\mathbf{P}\) the following identity holds
\cite[p. 87]{Bishop}
\begin{equation}
    \mathbf{P}^{-1} = 
    \begin{bmatrix}
        \mathbf{A} & \mathbf{B}\\
        \mathbf{C} & \mathbf{D}
    \end{bmatrix}^{-1}
    = 
    \begin{bmatrix}
        \mathbf{M}_D^{-1} & -\mathbf{M}_D^{-1}\mathbf{B}\mathbf{D}^{-1}\\
        -\mathbf{D}^{-1}\mathbf{C}\mathbf{M}_D^{-1} & 
        \mathbf{D}^{-1}+\mathbf{D}^{-1}\mathbf{M}_D^{-1}\mathbf{C}\mathbf{B}\mathbf{D}^{-1}
    \end{bmatrix}
\end{equation}

Thereby is \(\mathbf{M}_D = \mathbf{A}-\mathbf{B}\mathbf{D}^{-1}\mathbf{C}\) the 
Schur complement of \(\mathbf{P}\) with respect to block D.


\section*{Cholesky Decomposition}

For a symmetric positive definite matrix \(\mathbf{A}\), the Cholesky decomposition
is given by 
\begin{equation}
  \mathbf{A} = \mathbf{LL}^T
\end{equation}

Thereby, \(\mathbf{L}\) is a lower triangular matrix, that is called the 
Cholesky factor.
With this decomposition linear equation systems can be solved efficiently.
An equation of the form 
\begin{equation}
    \mathbf{A}\vec{x} = \vec{b}
\end{equation} is solved by first solving following equation for \(\vec{y}\) 
\begin{equation}
    \mathbf{L}\vec{y} = \vec{b}
\end{equation}
This is efficient as \(\mathbf{L}\) is a triangular matrix.
In a second step 
\begin{equation}
  \mathbf{L}^T\vec{x} = \vec{y}
\end{equation}

is solved for \(\vec{x}\) to obtain the final solution of the equation system.
This is especially useful, because it allows us to efficiently calculate
the inverse of the covariance matrix which is guaranteed to be symmetric.

The Cholesky decomposition can also be used to efficiently calculate the determinant
of a matrix. For the logarithmic determinant of a matrix we get the following expression
\begin{equation}
  \ln(\det(\mathbf{A})) = \ln(\det(\mathbf{L})^2) = 2\sum_i\ln(L_{ii})
\end{equation}

This is possible because the determinant of a triangular matrix is just given by 
the multiplication of its diagonal elements.

\subsubsection*{Numerical Stability}

In general the Cholesky decomposition is considered to be numerically stable. 
However, for very ill conditioned matrices the computation of the Cholesky factor
can still fail. In our specific case the condition number of the covariance 
matrix for the emission distribution \(\mathbf{\Sigma}_E\) which is 
given by the ratio of the largest and smallest eigenvalue was too large 
(\(\approx10^{15}\) or more) for larger length scales. Therefore, the inversion of the 
covariance matrix failed for length scales larger than 10 cm. This 
problem is solved by adding a small diagonal matrix to the covariance matrix.
This shifts the eigenvalues and decreases the condition number. In our case 
0.2 \% were added to the diagonal of the emission covariance matrix. The error 
introduced by this is negligible, but the condition number is lowered by several 
orders of magnitude.


\chapter{Conditional Mean and Covariance}
\label{appendix_conditional_mean_cov}

An extensive treatment of Gaussian distributions is given in 
\cite[section 2.3]{Bishop}

Let
\begin{equation}
  \begin{bmatrix}
      \vec{x}\\
      \vec{y}
  \end{bmatrix}
  \ \sim\ \mathcal{N}\left(
      \begin{bmatrix}
          \mathbf{\Sigma}_{xx} & \mathbf{\Sigma}_{xy}\\
          \mathbf{\Sigma}_{yx} & \mathbf{\Sigma}_{yy}
      \end{bmatrix}
  \right)
\end{equation}

be a joint normal distribution of both \(\vec{x}\) and \(\vec{y}\) with the 
following PDF.
\begin{equation}
    p(\vec{x}, \vec{y})\ \propto\ \exp\left[-\frac{1}{2}
    \begin{pmatrix}
        \vec{x}-\vec{\mu}_x\\
        \vec{y}-\vec{\mu}_y
    \end{pmatrix}^T
    \begin{bmatrix}
        \mathbf{\Sigma}_{xx} & \mathbf{\Sigma}_{xy}\\
        \mathbf{\Sigma}_{yx} & \mathbf{\Sigma}_{yy}
    \end{bmatrix}^{-1}
    \begin{pmatrix}
        \vec{x}-\vec{\mu}_x\\
        \vec{y}-\vec{\mu}_y
    \end{pmatrix}
    \right]
\end{equation}

By using the inversion formula for block matrices defined in appendix \ref{appendix_matrix_ident}
we can write
\begin{equation}
    \begin{bmatrix}
        \mathbf{\Sigma}_{xx} & \mathbf{\Sigma}_{xy}\\
        \mathbf{\Sigma}_{yx} & \mathbf{\Sigma}_{yy}
    \end{bmatrix}^{-1}
    = 
    \begin{bmatrix}
        \mathbf{\Sigma}_{xx}^* & \mathbf{\Sigma}_{xy}^*\\
        \mathbf{\Sigma}_{yx}^* & \mathbf{\Sigma}_{yy}^*
    \end{bmatrix}
\end{equation}

If we now look at the exponent of the PDF for the conditional distribution of 
\(\vec{x}|\vec{y}=\vec{a}\), we get
\begin{equation}\label{appendix_eq_cond_exp}
    \begin{split}
        \begin{pmatrix}
            \vec{x}-\vec{\mu}_x\\
            \vec{a}-\vec{\mu}_y
        \end{pmatrix}^T
        &\begin{bmatrix}
            \mathbf{\Sigma}_{xx}^* & \mathbf{\Sigma}_{xy}^*\\
            \mathbf{\Sigma}_{yx}^* & \mathbf{\Sigma}_{yy}^*
        \end{bmatrix}
        \begin{pmatrix}
            \vec{x}-\vec{\mu}_x\\
            \vec{a}-\vec{\mu}_y
        \end{pmatrix} \\
        =\ &(\vec{x}-\vec{\mu}_x)^T\mathbf{\Sigma}_{xx}^*(\vec{x}-\vec{\mu}_x)
        + (\vec{x}-\vec{\mu}_x)^T\mathbf{\Sigma}_{xy}^*(\vec{a}-\vec{\mu}_y)\\
        &+ \ (\vec{a}-\vec{\mu}_y)^T\mathbf{\Sigma}_{yx}^*(\vec{x}-\vec{\mu}_x)
        + (\vec{a}-\vec{\mu}_y)^T\mathbf{\Sigma}_{yy}^*(\vec{a}-\vec{\mu}_y)\\
        =\ &\vec{x}^T\mathbf{\Sigma}_{xx}^*\vec{x}\ - 
        \vec{x}^T[\mathbf{\Sigma}_{xx}^*\vec{\mu}_x-
        \mathbf{\Sigma}_{xy}^*(\vec{a}-\vec{\mu}_y)]\ +\ \dots
    \end{split}
\end{equation}

Ultimately the PDF of the conditioned distribution should take this form
\begin{equation}
    p(\vec{x}|\vec{y}=\vec{a})\ \propto\ \exp\left[-\frac{1}{2}(\vec{x}-\vec{\mu}_{x|a})^T
    \mathbf{\Sigma}_{x|a}^{-1}(\vec{x}-\vec{\mu}_{x|a})\right]
\end{equation}

With expanded exponent
\begin{equation}
    \vec{x}^T\mathbf{\Sigma}_{x|a}^{-1}\vec{x} - 
    \vec{x}^T\mathbf{\Sigma}_{x|a}^{-1}\vec{\mu}_{x|a} -
    \vec{\mu}_{x|a}^T\mathbf{\Sigma}_{x|a}^{-1}\vec{x} +
    \vec{\mu}_{x|a}^T\mathbf{\Sigma}_{x|a}^{-1}\vec{\mu}_{x|a}
\end{equation}

By comparing the first term of this exponent with the first of the last line of 
Eq. \ref{appendix_eq_cond_exp} we deduce 
\begin{equation}
    \mathbf{\Sigma}_{x|a} = [\mathbf{\Sigma}_{xx}^*]^{-1}
\end{equation}

and by comparing the second terms we find
\begin{equation}
    \mathbf{\Sigma}_{x|a}^{-1}\vec{\mu}_{x|a} = 
    \mathbf{\Sigma}_{xx}^*\vec{\mu}_x-\mathbf{\Sigma}_{xy}^*(\vec{a}-\vec{\mu}_y) 
    \quad \rightarrow \quad \vec{\mu}_{x|a} 
    = \vec{\mu}_x - [\mathbf{\Sigma}_{xx}^*]^{-1}\mathbf{\Sigma}_{xy}^*(\vec{a}-\vec{\mu}_y)
\end{equation}

According to the block matrix inversion formula, \([\mathbf{\Sigma}_{xx}^*]^{-1}\)
is given by the Schur complement of the original \(\mathbf{\Sigma}\) with 
respect to \(\mathbf{\Sigma}_{yy}\). By inserting the terms for 
\(\mathbf{\Sigma}_{xx}^*\) and \(\mathbf{\Sigma}_{xy}^*\) obtained from the 
inversion formula we get the terms for the conditional distribution expressed 
through terms of the joint distribution.
\begin{equation}
  \vec{\mu}_{x|a} = \vec{\mu}_x + 
  \mathbf{\Sigma}_{xy}\mathbf{\Sigma}_{yy}^{-1}(\vec{a}-\vec{\mu}_y)
\end{equation}
\begin{equation}
  \mathbf{\Sigma}_{x|a} = \mathbf{\Sigma}_{xx} - 
  \mathbf{\Sigma}_{xy}\mathbf{\Sigma}_{yy}^{-1}\mathbf{\Sigma}_{yx}
\end{equation}


\chapter{Marginalization of Linear Dependend Gaussian Variables}
\label{appendix_marg}

An extensive treatment of Gaussian distributions is given in 
\cite[section 2.3]{Bishop}

Let \(\vec{x}\) and \(\vec{y}\) be normally distributed random variables.
Now consider the normal distribution for \(\vec{x}\) and a conditional distribution 
for \(\vec{y}\) where the mean is given as a linear function of \(\vec{x}\).
\begin{equation}\label{appendix_eq_lin_dep}
  \vec{\mu}_{y|x} = \mathbf{A}\vec{x} + \vec{b}
\end{equation}
the PDFs for those distributions are
\begin{equation}
    p(\vec{x}) =  c_1\exp\left[-\frac{1}{2}(\vec{x}-\vec{\mu}_x)^T\mathbf{\Sigma}_x^{-1}
    (\vec{x}-\vec{\mu}_x)\right]
\end{equation}
\begin{equation}
    p(\vec{y}|\vec{x}) =  c_2\exp\left[-\frac{1}{2}(\vec{y}-\vec{\mu}_{y|x})^T
    \mathbf{\Sigma}_y^{-1}(\vec{y}-\vec{\mu}_{y|x})\right]
\end{equation}

\(c_1,c_2\) are the normalization constants of the distributions. By looking at 
the exponent of the joint PDF \(p(\vec{y}|\vec{x})p(\vec{x})\) and inserting 
Eq. \ref{appendix_eq_lin_dep} for \(\vec{\mu}_{y|x}\) we get 
\begin{equation}
    \begin{split}
        &-\frac{1}{2}\left[(\vec{x}-\vec{\mu}_x)^T\mathbf{\Sigma}_x^{-1}
        (\vec{x}-\vec{\mu}_x) + (\vec{y}-\mathbf{A}\vec{x}-\vec{b})^T\mathbf{\Sigma}_y^{-1}
        (\vec{y}-\mathbf{A}\vec{x}-\vec{b})\right]\\
        =
        &-\frac{1}{2}\left[\vec{x}^T(\mathbf{\Sigma}_x^{-1}+\mathbf{A}^T
        \mathbf{\Sigma}_y^{-1}\mathbf{A})\vec{x} + 
        \vec{y}^T\mathbf{\Sigma}_y^{-1}\vec{y} - 
        \vec{y}^T\mathbf{\Sigma}_y^{-1}\mathbf{A}\vec{x} -
        \vec{x}^T\mathbf{A}^T\mathbf{\Sigma}_y^{-1}\vec{y}\right] + C\\
        =
        &-\frac{1}{2}
        \begin{pmatrix}
            \vec{x} & \vec{y}
        \end{pmatrix}\underbrace{
        \begin{bmatrix}
            \mathbf{\Sigma}_x^{-1}+\mathbf{A}^T\mathbf{\Sigma}_y\mathbf{A} & 
            -\mathbf{A}^T\mathbf{\Sigma}_y^{-1} \\
            -\mathbf{\Sigma}_y\mathbf{A} & \mathbf{\Sigma}_y
        \end{bmatrix}}_{\mathbf{\Sigma}_{x\cup y}^{-1}}
        \begin{pmatrix}
            \vec{x}\\
            \vec{y}
        \end{pmatrix} + C
    \end{split}
\end{equation}

In the equation above \(C\) contains all the terms that are either constant or only 
linear in \(\vec{x}\) or \(\vec{y}\). \(\mathbf{\Sigma}_{x\cup y}\) is the covariance 
matrix of the joint distribution.
\begin{equation}
  \begin{bmatrix}
      \vec{x}\\
      \vec{y}
  \end{bmatrix}\
  \sim\ \mathcal{N}\left(
      \begin{bmatrix}
          \vec{\mu}_x\\
          \mathbf{A}\vec{\mu}_x + \vec{b}
      \end{bmatrix},
      \mathbf{\Sigma}_{x\cup y}
  \right)
\end{equation}

To obtain the covariance of the joint distribution we use the matrix inversion
formula for block matrices described in appendix \ref{appendix_matrix_ident}.
\begin{equation}
    \mathbf{\Sigma}_{x\cup y} = 
    \begin{bmatrix}
        \mathbf{\Sigma}_x & \mathbf{\Sigma}_x\mathbf{A}^T \\
        \mathbf{A}\mathbf{\Sigma}_x & \mathbf{\Sigma}_y + 
        \mathbf{A}\mathbf{\Sigma}_x\mathbf{A}^T
    \end{bmatrix}
\end{equation}

According to section \ref{kap2_marginalization} the distribution for \(\vec{y}\)
is given by
\begin{equation}
  \vec{y}\ \sim\ \mathcal{N}(\mathbf{A}\vec{\mu}_x + \vec{b},\ \mathbf{\Sigma}_y + 
  \mathbf{A}\mathbf{\Sigma}_x\mathbf{A}^T)
\end{equation}

with the PDF
\begin{equation}
    \begin{split}
        p(\vec{y})\ =\ &\frac{1}{\sqrt{(2\pi)^N\det(\mathbf{\Sigma}_y + 
        \mathbf{A}\mathbf{\Sigma}_x\mathbf{A}^T)}}\\
        &\exp\left[-\frac{1}{2} (\vec{y}-\mathbf{A}\vec{\mu}_x-
        \vec{b})^T[\mathbf{\Sigma}_y + 
        \mathbf{A}\mathbf{\Sigma}_x\mathbf{A}^T]^{-1}(\vec{y}-
        \mathbf{A}\vec{\mu}_x-\vec{b}) \right]
    \end{split}
\end{equation}

Thus, the logarithmic likelihood of \(p(\vec{y})\) is given by 
\begin{equation}
    \begin{split}
        \ln(p(\vec{y}))\ =\ & -\frac{N}{2}\ln(2\pi)
        -\frac{1}{2}\ln(\det(\mathbf{\Sigma}_y+\mathbf{A}\mathbf{\Sigma}_x\mathbf{A}^T))\\
        &-\frac{1}{2}(\vec{y}-\mathbf{A}\vec{\mu}_x-\vec{b})^T[\mathbf{\Sigma}_y + 
        \mathbf{A}\mathbf{\Sigma}_x\mathbf{A}^T]^{-1}(\vec{y}-
        \mathbf{A}\vec{\mu}_x-\vec{b}) 
    \end{split}
\end{equation}

where \(N\) is the number of random variables in \(\vec{y}\).