\chapter{Introduction}
\section{Nuclear Fusion}

The demand for energy is rising in our industrialized and globalized world. 
In 2018 it was estimated that roughly 23000 TWh of electrical energy were consumed 
worldwide and alone from 1990 to 2013 the worldwide consumption doubled from 
10000 to 20000 TWh \cite{enerdata}. Even though renewable energy sources are on the rise 
and consituted 25\% of the worldwide electricity production in 2017, still more than 60\% 
came from fossil fuels \cite{iea}. The pressing issue of climate change 
and also the limited availability of those fuels require the search for 
new energy sources. 
One such energy source can be nuclear fusion. Compared to conventional 
chemical reactions, occuring when burning fossil fuels, nuclear reactions produce roughly 
a million times more energy per reaction than their chemical counterparts. 
Eq. \ref{kap1_eq_nuclear_chemical} compares the chemical reaction of a hydrogen 
oxygen mixture also known as "Knallgas" to nuclear reactions. While for the 
oxyhydrogen reaction only a couple of electron volts are released, nuclear reactions
like the nuclear fission of Uranium-235 and the fusion of the two hydrogen isotopes 
Deuterium and Tritium releases several MeV of energy per reaction. 

\begin{subequations}\label{kap1_eq_nuclear_chemical}
    \begin{align} 
        \mathrm{2H_2 + O_2} &\rightarrow \mathrm{2H_2O +  5.9\ eV} \\
        \mathrm{n + {}_{92}^{235}U} &\rightarrow \mathrm{{}_{56}^{141}Ba + 
        {}_{36}^{92}Kr + 3n + 202.5\ MeV} \\
        \mathrm{D+T} &\rightarrow \mathrm{{}_2^4He + n + 17.6\ MeV}
    \end{align}    
\end{subequations}

The second reaction 
is already utilized in nuclear fission power plants. While energy production 
with nuclear fission is less harmful to the environment than fossil fuels and the 
total amount of energy available through Uranium or other nuclear fuels far exceeds 
those of conventional fuels due to the much higher energy density, the disposal 
of nuclear waste is still an unsolved problem and disasters like Chernobyl or Fukushima 
illustrate the hazard potential of such power plants. Nuclear fusion on the other hand 
does not produce nuclear waste radiating for millions of years and a possibility 
for serious accidents is not given, as nuclear fusion has to be actively maintained 
and is not a chain reaction like fission. There are other possible fusion reactions 
like the fusion of two Deuterium nuclei or one Deuterium and one Helium-3 nuclei, 
but the reaction of Deuterium and Tritium has the highest cross-section at lower temperatures 
and is therefore most promissing (see Fig. \ref{kap1_img_crosssections}). 
While Deuterium is abundant and occurs naturally as 0.015\% of all 
hydrogen isotopes, Tritium being radioactive with a half-time of roughly 12 years 
does not occur naturally and would have to be bred from Lithium.

In order to achieve conditions for nuclear 
fusion two different approaches are currently evaluated. 
\begin{itemize}
    \item The first is inertial 
    confinement fusion. Thereby a small solid target containing the Deuterium Tritium 
    mixture is compressed and heated up by lasers or particle beams on a short time scale
    (\(\approx 10^{-10}\) s).
    \item The second approach is magnetic confinement. Here the plasma is confined by a magnetic 
    field through the Lorentz force. One of the most promissing configurations 
    for magnetic confinement is the Tokamak described in the next section. 
    The target parameters for having a net energy gain of such a device are 
    roughly given by a particle density of \(n\approx 10^{20}\ \mathrm{m}^{-3}\), 
    a temperature of \(T \approx 15\ \mathrm{keV}\), which is equal to 
    approximately 150-200 million Kelvin,
    and a confinement time of 5 seconds or more \cite{scriptPlasma}.
\end{itemize} 
 


\begin{figure}[H]
    \centering
    \includegraphics[width=0.65\textwidth]{img/fusion_cross_section.png}
    \caption[Fusion reaction cross-sections]
    {Cross section of different fusion reactions. The Deuterium Tritium 
    reaction has a higher cross-section at lower energies. The data was taken from 
    the ENDF-Database.\protect\footnotemark\({}^,\)\protect\footnotemark}
    \label{kap1_img_crosssections}
\end{figure}

\footnotetext[1]{\url{https://scipython.com/blog/plotting-nuclear-fusion-cross-sections/}}
\footnotetext[2]{\url{https://www-nds.iaea.org/exfor/endf.htm}}

\section{Tokamak}

The term Tokamak is a Russian acronym for 
\textit{"toroidalnaja kamera w magnitnych katuschkach"} which translates into 
"toroidal chamber in magnetic coils"

To achieve magnetic confinement, the Tokamak concept consists of three different 
magnetic fields that are superposed.

\begin{itemize}
    \item First toroidal field coils are arranged around the toroidal chamber.
    These produce a field inside the Tokamak that confines the plasma particles
    to trajectories within the chamber as the Lorentz force makes them 
    spiral with small radii around the field lines compared to the radius of 
    the chambers cross-section. Because the coils are arranged circularly around 
    the chamber, they produce a bend magnetic field where the 
    magnetic flux density is larger on the inside than on the outside. Therefore, 
    the magnetic field is not homogeneous and the resulting gradient of the 
    magnetic field causes a drift of the plasma.
    \item To prevent this drift a current is induced into the plasma. This is 
    achieved by coils in the center of the torus that act as primary transformator 
    coils. The magnetic field caused by the current in addition with the field of 
    the toroidal field coils results in a field lines that screw around the torus.
    \item Additional vertical coils can be used to modify the shape or the placement
    of the plasma in the toroidal chamber.
\end{itemize}

Fig. \ref{kap1_img_tokamak_fields} shows the arrangement of the field coils for 
a Tokamak.

\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{img/tokamak_prinzip.png}
    \caption[Tokamak schematic]
    {Schematic drawing of a Tokamak. The magnetic field of the toroidal 
    field coils combined with the field of the plasma current induced by the 
    transformator coils lead to a field that screws around the torus. \tiny{
    (Source: \url{https://www.ipp.mpg.de/14869/tokamak})} }
    \label{kap1_img_tokamak_fields}
\end{figure}

In order to sustain the fusion reaction the fusion power has to be larger than 
the power loss
\begin{equation}
  P_{fusion} > P_{conduction\ loss} + P_{radiation\ loss}
\end{equation}

The conduction loss is given by particles leaving the plasma (e.g. through 
interacting with the walls of the plasma vessel). The radiation loss is given 
through the photons emitted by the plasma. It consists of different components 
like bremsstrahlung, line radiation or radiation through recombination. One 
factor that influences the loss through radiation are impurities in the plasma. 
These can for example be given by atoms from the plasma vessel walls. 

A self sustaining fusion plasma has not yet been achieved by any experiment 
based on magnetic confinement as the particle densities and the 
confinement times are too low on those devices.

Currently the largest Tokamak is the Japanese JT-60SA, whose construction was 
just finished while this thesis was written. With a volume of 130 m³ 
it has approximately a sixth of the volume of ITER (International Thermonuclear 
Experimental Reactor) that is currently under construction in France. 
ITER is supposed to be the first Tokamak with a net energy gain, producing 500 MW 
while only using 50 MW for heating the plasma \cite{ippJT60SA}.

The previously largest Tokamak experiment JET (Joint European Torus) in Great Britain
already showed that it could produce 65\% of its heating power with fusion. Due 
to copper coils the plasma discharges at JET are below one minute. Therefore, both 
JT-60SA and ITER are equipped with supra-conductive coils enabling much 
longer times for discharges \cite{ippJT60SA}.

There are several other smaller Tokamak experiments around the world providing
the database for constructing ITER and testing individual parts and components to 
be used in ITER. However, none of them is large enough to reach the required parameters 
for a net energy gain and mostly work with model plasmas consisting only of 
Deuterium or Helium.

One of those Tokamaks is ASDEX Upgrade (AUG) located in Garching Germany.
With a major radius of 1.65 m, a minor radius of 0.5 m and a plasma volume of 13 m²
AUG is a medium sized Tokamak experiment of about one fourth the size of ITER.

\section{Bolometer Diagnostic}

Bolometers are one of the standard diagnostics for fusion experiments. 
A bolometer is a sensor/camera that measures the total radiation along one sight line.
In order to get information about the spatial distribution the radiation is 
measured for numerous sight lines with different positions 
and angles. The spatial distribution can then be calculated with tomography 
algorithms. Compared to other diagnostics like the commonly used soft X-ray 
diagnostic that only measures radiation in the X-ray range, bolometers measure 
the radiation over the whole emissivity spectrum.

AUG has two different bolometer systems. The first consists of foil Bolometers.
In order to prevent distortion of the emission, no optics are used and the bolometers 
are directly installed in the plasma vessel with a pinhole geometry to limit 
the radiation to one sight line \cite{BolometerPHD}. A foil bolometer 
consists of three layers. The first is a thin metallic foil (often gold) absorbing the radiation
which increases its temperature. The second layer given by an electrically insulating 
substrate like Mika or Kapton then transfers the heat to the third layer consisting 
of a thin meandered conductor with a high resistance of a few \(\mathrm{k\Omega}\).
The radiation power can be obtained by measuring 
the resistance of this layer via a Wheatstone bridge which is proportional to 
the temperature.
Therefore an expression for the radiation power along the sight line depended on the 
voltage across the bridge is given by \cite{BolometerPaper}
\begin{equation}
  P_{radiation} = C\left(\frac{dU}{dt} + \frac{U}{\tau}\right)
\end{equation}

Here \(C\) is a calibration factor containing the heat capacity and resistances
of the used materials and \(\tau\) is a cooling time constant.

Additionally AUG has a bolometer diagnostic based on photodiodes. The advantage 
of the photodiodes is that they have a higher time resolution 
than foil bolometers as the time for the heat transport is limiting them. 
On the other hand calibration of the diodes is difficult, as the sensitivity 
of the diodes varies for different wavelengths \cite{BolometerDiplom}.

In this thesis only the data from the foil bolometers is used. 
The foil bolometer diagnostic on AUG consists of 6 cameras with a total 
of 128 sight lines (see Fig. \ref{kap1_AUG_bolometer_channels}).   

\begin{figure}[H]
    \centering
    \includegraphics[width=0.35\textwidth]{img/bolometer_channels.png}
    \caption[AUG Bolometer channels]{All 128 bolometer channels at ASDEX Upgrade}
    \label{kap1_AUG_bolometer_channels}
\end{figure}