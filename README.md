# Bachelor Thesis: Gaussian Processes for Emission Tomography at ASDEX Upgrade

In this thesis Gaussian process regression was applied to the bolometer 
diagnostic of ASDEX Upgrade to calculate emission distributions from given 
bolometer measurements.

The Code for the thesis can be found in the Code folder along with a 
short python script "example.py" to demonstrate the algorithm on one test 
sample. The script should produce the following plot:

![alt](Code/data/READMEpic.png)


The only dependencies needed are numpy, scipy and matplotlib.
The Eigen3 C++ library is distributed with the code. You might have to 
recompile the code for your target machine with the supplied makefile. 